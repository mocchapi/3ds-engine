# 3DS engine
(working title)  
Lightweight game engine for creating Nintendo 3DS homebrew games & software.  
Inspired by & made with [Godot engine 4.2](https://godotengine.org)

## WIP
This project is in its infancy and Literally Does Not Work At All yet

## Attribution

This project includes several binary builds of various projects for its build system:

- [Luna Player Plus 3DS](https://github.com/Rinnegatamante/lpp-3ds) by Rinnegatamante <sup>([view license](src/bundled/buildtools/lpp-3ds/LICENSE.txt))</sup>
- [MakeROM](https://github.com/3DSGuy/Project_CTR/tree/master/makerom) by 3DSGuy <sup>([view license](src/bundled/buildtools/makerom/LICENSE.txt))</sup>
- [bannertool](https://github.com/Steveice10/bannertool) by Steveice10 <sup>([view license](src/bundled/buildtools/bannertool/LICENSE.txt))</sup>

It also uses the following godot add-ons:
- [godot-uuid](https://github.com/binogure-studio/godot-uuid) by Xavier Sellier <sup>([view license](https://github.com/binogure-studio/godot-uuid/blob/master/LICENSE))</sup>
- [lua-gdextension](https://github.com/gilzoide/lua-gdextension) by Gil Barbosa Reis <sup>([view license](https://github.com/gilzoide/lua-gdextension/blob/main/LICENSE))</sup>
