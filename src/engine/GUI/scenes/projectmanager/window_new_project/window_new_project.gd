extends Window

signal project_created(project:Project)

@onready var line_newproject_name = %line_newproject_name
@onready var line_newproject_path = %line_newproject_path
@onready var lbl_newproject_error = %lbl_newproject_error
@onready var btn_newproject_create = %btn_newproject_create

func _ready():
	_on_visibility_changed()
	verify_paths()

func _on_visibility_changed():
	if line_newproject_name:
		line_newproject_name.clear()
		line_newproject_path.text = OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS)
		btn_newproject_create.disabled = true
		lbl_newproject_error.text = ""
		if visible:
			line_newproject_name.grab_focus()

func verify_paths()->bool:
	var project_path = line_newproject_path.text
	var project_name = line_newproject_name.text 
	
	btn_newproject_create.disabled = true
	if project_name == "":
		lbl_newproject_error.text = "Project name cannot be empty"
		return false
	elif project_path == "":
		lbl_newproject_error.text = "Project path cannot be empty"
		return false
	elif not DirAccess.dir_exists_absolute(project_path):
		if FileAccess.file_exists(project_path):
			lbl_newproject_error.text = 'Path "{0}" is a file, not a directory'.format([project_path])
		else:
			lbl_newproject_error.text = 'Path "{0}" does not exist'.format([project_path])
		return false
	btn_newproject_create.disabled = false
	lbl_newproject_error.text = ""
	return true

func _on_line_newproject_path_text_changed(new_text):
	verify_paths()

func _on_line_newproject_name_text_changed(new_text):
	verify_paths()

func _on_filebutton_selected(path):
	line_newproject_path.text = path
	verify_paths()


func _on_btn_newproject_createfolder_pressed():
	var project_path = line_newproject_path.text
	var project_name = line_newproject_name.text 
	
	var new_path:String
	if project_path.ends_with(project_name):
		new_path = project_path
	elif not project_path.ends_with('/'):
		new_path = project_path + '/' + project_name
	else:
		new_path = project_path + project_name
	
	line_newproject_path.text = new_path
	if DirAccess.make_dir_recursive_absolute(new_path) != OK:
		lbl_newproject_error.text = "Could not make folder :("
		return
	else:
		verify_paths()


func _on_btn_newproject_create_pressed():
	var project_path = line_newproject_path.text
	var project_name = line_newproject_name.text 
	project_created.emit(
		ProjectFactory.create(project_path, Project.ProjectConfig.new(project_name))
		)
	hide()
	
