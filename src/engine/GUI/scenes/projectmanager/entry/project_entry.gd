extends MarginContainer

@onready var img_icon = %img_icon
@onready var lbl_title = %lbl_title
@onready var lbl_path = %lbl_path
@onready var lbl_author = %lbl_author
@onready var lbl_edited = %lbl_edited
@onready var bg_if_selected = $bg_if_selected

signal selected(entry)

@export var project:Project:
	set = set_project

@export var is_selected:bool = false:
	set(new):
		is_selected = new
		if bg_if_selected != null:
			bg_if_selected.visible = new

func _ready():
	set_project(project)

func set_project(new:Project):
	project = new
	if img_icon:
		lbl_title.text = project.config.info_name
		lbl_path.text = project.path
		lbl_author.text = "by "+project.config.info_author
		lbl_edited.text = "modified on "+Time.get_datetime_string_from_unix_time(project.config.meta_edited, true)


func _on_button_pressed():
	selected.emit(self)
	pass # Replace with function body.
