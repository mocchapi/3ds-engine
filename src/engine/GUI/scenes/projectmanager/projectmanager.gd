extends Control

@onready var window_new_project = $window_new_project
@onready var entry_holder = %entry_holder

@onready var btn_edit = %btn_edit
@onready var btn_delete = %btn_delete
@onready var btn_view_files = %btn_view_files



var entry_scene := preload("res://src/engine/GUI/scenes/projectmanager/entry/project_entry.tscn")

var selected_entry:
	set(entry):
		if selected_entry != null:
			selected_entry.is_selected = false
		selected_entry = entry
		if entry != null:
			entry.is_selected = true
		if btn_delete:
			btn_delete.disabled = entry == null
			btn_edit.disabled = entry == null
			btn_view_files.disabled = entry == null

func _ready():
	var list:Array = FileUtils.read_or_create_JSON(
		Constants.ProjectsList, [])
	if list.is_empty():
		Logger.info("No projects exist yet")
		window_new_project.show()
	else:
		# Check if all the project files exist still
		# Purge them if they dont
		Logger.debug("Checking if all registered projects still exist")
		var prevlen = len(list)
		for idx in range(prevlen,0,-1):
			if not FileUtils.exists(list[idx-1], false, true):
				Logger.warningP("Project no longer exists:")
				Logger.warningE(list[idx-1])
				list.remove_at(idx-1)
		if prevlen != len(list):
			FileUtils.write_JSON(Constants.ProjectsList, list)
		# Build the GUI for each entry
		rebuild_project_list(list)

## Constructs the project entries in the GUI
func rebuild_project_list(project_paths=null):
	Logger.debug("Rebuilding project list")
	if project_paths == null:
		project_paths = FileUtils.read_or_create_JSON(
			Constants.ProjectsList, [])
	else:
		Logger.debug("Received custom list of "+str(len(project_paths))+" items")
	
	selected_entry = null
	
	for child in entry_holder.get_children():
		Logger.debugP("Freeing previous child "+str(child))
		entry_holder.remove_child(child)
		child.queue_free()
	
	
	for item in project_paths:
		if not FileUtils.exists(item, false, true):
			continue
		var project = ProjectFactory.load(item)
		var new = entry_scene.instantiate()
		Logger.debugP("Creating entry for project: "+str(project.config.info_name))
		Logger.debugE(item)
		new.project = project
		new.selected.connect(_on_entry_selected)
		entry_holder.add_child(new)

func _on_entry_selected(entry):
	selected_entry = entry

func _on_window_new_project_project_created(project):
	rebuild_project_list()

func _on_btn_deselect_selection_pressed():
	selected_entry = null

func _on_window_import_project_file_selected(path):
	var list = FileUtils.read_or_create_JSON(Constants.ProjectsList, [])
	if path in list:
		Logger.warning("Attempted to import project "+path+", but its already on the list.")
		return
	
	Logger.info("Imported project "+path)
	var newlist = [path] + list
	FileUtils.write_JSON(Constants.ProjectsList, newlist)
	rebuild_project_list(newlist)


func _on_btn_edit_pressed():
	if selected_entry:
		Switcher.edit_project(selected_entry.project)


func _on_btn_view_files_pressed():
	if selected_entry:
		OS.shell_show_in_file_manager(selected_entry.project.path)
