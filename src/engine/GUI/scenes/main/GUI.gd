extends Control
@onready var main = $".."

func _unhandled_input(event):
	return
	if event.is_action_released("save", true):
		get_viewport().set_input_as_handled()
		Logger.debug("Caught ctrl+s")
		main.save()
