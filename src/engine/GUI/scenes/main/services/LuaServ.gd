extends Node
class_name LuaServ

signal executed(result:LuaServResult)

@export var executions_per_second:float = 25

var default_libraries:int = LuaState.LUA_STRING | LuaState.LUA_BASE | LuaState.LUA_MATH | LuaState.LUA_DEBUG | LuaState.LUA_TABLE

## Current queue
var queue:Array[LuaQueueItem] = []
## Current ticket index
var idx:int = 0
## Counted seconds, when it reaches equal or bigger than `1/executions_per_second` it will trigger an execution
var timer:float = 0

class LuaQueueItem:
	var ticket:int = 0
	var libraries:int = 0
	var code:String
	func _init(ticket:int, libraries:int, code:String):
		self.ticket = ticket
		self.libraries = libraries
		self.code = code

class LuaServResult:
	var ticket:int = 0
	var libraries:int = 0
	var result = null
	func _init(ticket:int, libraries:int, result):
		self.ticket = ticket
		self.result = result
		self.libraries = libraries

## Place lua string in the process queue. You can await the result with `await await_ticket(queue_string(code))`
func queue_string(code:String, libraries:int=default_libraries)->int:
	idx+=1
	queue.append(LuaQueueItem.new(idx, libraries, code))
	return idx

func _process(delta):
	if queue.is_empty():
		timer = 1/executions_per_second
		return
	timer += delta
	while timer >= (1/executions_per_second) and not queue.is_empty():
		timer -= (1/executions_per_second)
		var item:LuaQueueItem = queue.pop_back()
		Logger.debug("Processing "+str(item.ticket))
		var result = execute_string(item.code, item.libraries)
		if result is LuaTable:
			#Logger.debug("Result: "+str(result.to_dictionary()))
			Logger.debug("Result: "+str(result.to_dictionary()['__meta__'].to_dictionary()['exports'].to_dictionary()))
			pass
		else:
			pass
			Logger.debug("Result: "+str(result))
			
		executed.emit(LuaServResult.new(item.ticket, item.libraries, result))

func execute_string(code:String, libraries:int=default_libraries):
	Logger.debug("Executing:")
	var ctx := LuaState.new()
	ctx.open_libraries(libraries)
	return ctx.do_string(code)

func await_ticket(ticket:int, max_waits=500):
	var idx := 0
	while true:
		var out = await executed
		if out.ticket == ticket:
			return out
		else:
			idx += 1
			if idx > max_waits:
				Logger.error("Ticket "+str(ticket)+" was not found after 500 queue entries!")
				return null
