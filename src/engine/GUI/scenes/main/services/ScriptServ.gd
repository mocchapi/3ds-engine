extends Node
class_name ScriptServ

@onready var filedb = %FileDB
@onready var luaserv = %LuaServ

signal wobbled(file:FileDB.ProjectFileUUID)

var cache:Dictionary = {}

var TEMPLATE_USERSCRIPT:String = FileUtils.read("res://src/lua/scriptserv/template_userscript.lua")
var TEMPLATE_INSPECTION:String = FileUtils.read("res://src/lua/scriptserv/template_inspection.lua")
var template_key_code := "{{{INSERTCODE}}}"
var template_key_name := "{{{INSERTNAME}}}"

class InspectionResult:
	var lua_result
	var is_error:bool = false
	func _init(lua_result):
		self.lua_result = lua_result

class InspectionError: 
	extends InspectionResult
	var line:int
	var message:String
	func _init(lua_result, message=null, line=null, line_offset:=0):
		assert(lua_result is LuaError)
		super(lua_result)
		is_error = true
		if message == null:
			message = lua_result.message
			
		# Parses line numbers in error message
		# If something in the guts wants to error on a specific line in the script source
		# it can do so by starting the error with `@>>12<<@` where 12 is replaced with the wanted line number
		# if theyre not present itll just use the normal line number as reported by lua
		var split = message.split(']:', true, 1)
		if line == null:
			if '@>>' in split[1] and '<<@' in split[1]:
				message = split[1].split(':', 1)[1]
				split = split[1].split('<<@', true, 1)
				message = split[1]
				line = int(split[0].split('@>>', true, 1)[1])
				Logger.warning("FANCY TOWN!! "+str(line)+' (-'+str(line_offset)+' )!!! '+str(message))
			else:
				split = split[1].split(':', 1)
				message = split[1]
				line = int(split[0])
		else:
			message = split[1].split(':', 1)[1]
		self.line = line-line_offset
		self.message = message

class InspectionSuccess:
	extends InspectionResult
	
class InspectionScriptSuccess:
	extends InspectionSuccess
	var exports: Array
	var inherits:String
	var signals: Array
	var ancestors: Array
	func _init(lua_result, exports=null, inherits=null, signals=null, ancestors=null):
		assert(lua_result is LuaTable)
		super(lua_result)
		if exports == null:
			exports = lua_result['__meta__']['exports'].to_array()
		if inherits == null:
			inherits = lua_result['__meta__']['inherits']
			if inherits == null:
				inherits = ''
		if signals == null:
			signals = lua_result['on'].to_array()
			if signals == null:
				signals = []
		if ancestors == null:
			ancestors = lua_result['__meta__']['ancestors'].to_array()
		self.exports = exports
		self.inherits = inherits
		self.signals = signals
		self.ancestors = ancestors

func build(code:String, classname:String):
	return TEMPLATE_USERSCRIPT.replace(template_key_name, classname).replace(template_key_code, code)


func inspect(code:String, classname:String, is_userscript:=true):
	Logger.debugP('inspecting "'+classname+'": '+code.substr(0,30).replace('\n','\\n')+'...')
	
	var line_offset:int = 1 
	var search_template = TEMPLATE_INSPECTION

	if is_userscript:
		# If this is a userscript, it has to be converted to CLASS(func...) format first
		search_template = search_template.replace(template_key_code, TEMPLATE_USERSCRIPT)
		code = build(code, classname)

	var code_offset = search_template.find(template_key_code)
	# find how many lines down {{{INSERTCODE}}} is for tracebacks
	line_offset = search_template.countn('\n', 0, code_offset)
	# Apply the template
	code = TEMPLATE_INSPECTION.replace(template_key_code, code)
	
	#Logger.debug(code)
	var result = await luaserv.await_ticket(luaserv.queue_string(code))
	assert(result.result != null, "Lua result is null! did you forget to return a value in the file?")
	
	if result.result is LuaError:
		Logger.debug(result.result.message)
		return InspectionError.new(result.result, null, null, line_offset)
	elif classname.ends_with('.script'):
		return InspectionScriptSuccess.new(result.result)
	else:
		return InspectionSuccess.new(result.result)

func ingest(file:FileDB.ProjectFileUUID):
	cache[file.get_uuid()] = await inspect(file.read(), file.path, not file.internal_file)
	wobbled.emit(file)

func drop(uuid:String):
	if cache.has(uuid):
		Logger.debug("Dropping "+uuid)
		cache.erase(uuid)
	else:
		Logger.warning("Can't drop "+uuid+": not in cache")

func get_inspection(uuid:String)->InspectionResult:
	return cache.get(uuid)

func _on_file_db_deleted(file:FileDB.ProjectFile):
	if file.type == FileDB.FILETYPE.SCRIPT:
		drop(file.get_uuid())

#func _on_file_db_created(file:FileDB.ProjectFile):
	#if file.type == FileDB.FILETYPE.SCRIPT:
		#ingest(file)
#
#func _on_file_db_moved(file:FileDB.ProjectFile, old_path):
	#if file.type == FileDB.FILETYPE.SCRIPT:
		#ingest(file)
#
#func _on_file_db_modified(file:FileDB.ProjectFile):
	#if file.type == FileDB.FILETYPE.SCRIPT:
		#ingest(file)


func _on_file_db_wobbled(file):
	if file.type == FileDB.FILETYPE.SCRIPT:
		ingest(file)



func _on_file_db_clean_indexed():
	for file in filedb.uuids.values():
		if file.type == FileDB.FILETYPE.SCRIPT:
			ingest(file)
