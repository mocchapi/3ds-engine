extends Node
class_name FileDB

## Indexes & identifies all project files.
## ProjectFile & ProjectFileProxy offer an object-oriented way to interact with the filesystem

## ProjectFile allows actual file operations (read, write, move, etc), but should be used for as little time as possible.Completely avoid storing references to it, they will get stale.
## ProjectFileProxy offers signals to detect changes, as well as a safer way to get to the ProjectFile.

## The preferred method to use these two classes is to store a ProjectFileProxy to the file you are working with (you can get it through get_proxy()), and then use myproxy.solidify().myoperation() for each ProjectFile operation you must do.


signal clean_indexed()
signal indexed()

## Catch-all signal, emits for `modified`, `moved`, & `created`
signal wobbled(file:ProjectFile)

signal modified(file:ProjectFile)
signal deleted(file:ProjectFile)
signal moved(file:ProjectFile, old_path:String)
signal created(file:ProjectFile)

@onready var main:Main = $/root/main


enum FILETYPE {
	SCRIPT,		## .script script file
	SCENE,		## .scene file
	TEXTURE,	## Any supported image
	AUDIO,		## Any supported sound
	DIRECTORY,	## Folder
	UNKNOWN,	## Idk what this file is
	INVALID,	## File not found, or otherwise messed up
}

## Dict that maps a FILETYPE to an array of file extensions
@export var file_associations:Dictionary = {
	FILETYPE.SCRIPT:	['.script'],
	FILETYPE.SCENE:		['.scene'],
	FILETYPE.TEXTURE:	['.png', '.jpeg', '.jpg'],
	FILETYPE.AUDIO:		['.ogg','.wav'],
}

## All indexed files, mapped by project-local path
var files:Dictionary = {}
## All indexed scripts & scenes, mapped by their UUIDs
var uuids:Dictionary = {}

class ProjectFile extends Resource:
	## Object represents 
	
	## Project-relative path of this file. IE "source/scripts/new.script"
	@export var path:String:
		get = get_local_path
	@export var type:FILETYPE
	## Most non-source/ files and directories, like "pipelines/" & "project.json" will have this set to true.
	## Should be hidden from the user
	@export var hidden_file:bool=false
	## Special "internal" files. These only exist inside res:// instead of on the filesystem
	## Cannot be written to.
	@export var internal_file:bool=false
	## Some warning text to show to the user?
	@export var warning:String = ""
	## Metadata linked to this file. Can get purged at any time & does not persist
	@export var metadata:Dictionary = {}
	## Hash from last time this file was written/read
	var hash

	## The fileDB this belongs to
	var filedb:FileDB
	var _contents
	
	func _init(filedb:FileDB, path:String, type:FILETYPE, hidden_file:bool=false, internal_file:bool=false):
		self.path = path
		self.filedb = filedb
		self.type = type
		self.hidden_file = hidden_file
		self.internal_file = internal_file
	
	func get_local_path()->String:
		return path
	
	func can_have_uuid()->bool:
		return type == FILETYPE.SCRIPT or type == FILETYPE.SCENE or internal_file
	
	## Returns the absolute filesystem path, for doing operations
	func get_absolute_path()->String:
		if internal_file:
			return path
		return filedb.main.project.path + path
	
	## Returns either the directory path this file is located in, or the same path if this file is a directory
	func get_closest_directory(absolute:=false)->String:
		var out:String
		if type == FILETYPE.DIRECTORY:
			out = path
		else:
			out = path.get_base_dir()
		
		if not out.ends_with('/'):
			out += '/'

		if absolute and not internal_file:
			return filedb.main.project.path + out
		else:
			return out
	
	## Returns the content's hash as it is in the cache
	## <br>To update the hash, call ProjectFile.read(false) first
	func get_hash():
		if hash != null:
			return hash
		read(false)
		return hash

	## Attempt to open this file in the default external program, like an image editor.
	## Returns an `Error`
	func open_external()->int:
		Logger.info("Opening "+get_absolute_path()+" in external program")
		return OS.shell_open(get_absolute_path())
	
	func open_internal()->bool:
		## Attempt to open this file inside one of the editors, like Scenes or Scripts
		match type:
			FILETYPE.SCRIPT:
				filedb.main.scripts.open(path)
				filedb.main.interfaces.show_scripts()
			FILETYPE.SCENE:
				filedb.main.scenes.open(path)
				filedb.main.interfaces.show_scenes()
		return false
	
	func open_directory()->int:
		## Open the directory that contains this file in the system file explorer
		Logger.info("Opening "+get_absolute_path()+"'s directory in system file explorer")
		return OS.shell_show_in_file_manager(get_absolute_path())
	
	## Write a string to this file, replacing its original contents
	func write(contents:String)->bool:
		if is_directory() or internal_file:
			return false
		_contents = contents
		self.hash = filedb.hashthis(contents)
		if filedb._write_to(path, contents):
			filedb.modified.emit(self)
			filedb.wobbled.emit(self)
			return true
		return false

	func is_file()->bool:
		return not is_directory() and type != FILETYPE.INVALID
	
	func is_directory()->bool:
		return type == FILETYPE.DIRECTORY

	## Return the file contents as string, uses a cache by default
	func read(use_cache:=false)->String:
		if is_directory():
			return ""
		if use_cache and _contents != null:
			return _contents
		_contents = FileUtils.read(get_absolute_path())
		self.hash = filedb.hashthis(_contents)
		return _contents
	
	func get_friendly_type()->String:
		return str(FILETYPE.keys()[type]).capitalize()
	
	func _to_string()->String:
		return '<'+path+';'+get_friendly_type()+';'+str(get_instance_id())+'>'
	
	func _invalidate_cache():
		_contents = null
	
	func proxy()->ProjectFileProxy:
		return ProjectFileProxy.new(filedb, self.path)

class ProjectFileUUID extends ProjectFile:
	## File that contains an *EMBEDDED* unique identifier. persists between writes & reads
	var uuid:
		set(new):
			if new != uuid:
				if uuid != null:
					Logger.warning(str(self)+" UUID re-assign! "+str(uuid)+' to '+str(new))
				uuid = new

	const UUID_START = "START-UUID"
	const UUID_END = "END-UUID"
	const UUID_SNIPPET = "--# START-UUID {uuid} END-UUID // Do NOT change, move, or delete this line!!"

	func _init(filedb:FileDB, path:String, type:FILETYPE, hidden_file:bool=false, internal_file:bool=false, uuid=null):
		super(filedb, path, type, hidden_file, internal_file)
		if uuid:
			self.uuid = uuid
	
	func get_uuid()->String:
		if uuid != null:
			#Logger.debug(str(self)+" UUID FROM CACHE: "+uuid)
			return uuid
		if internal_file:
			## Internal scripts are read-only
			## So we just use a hash of the path as a uuid
			uuid = filedb.hashthis(path.substr(len(Constants.EngineLuaDirectory)))
			return uuid
		read(false)
		if uuid == null:
			uuid = filedb._fresh_uuid()
			Logger.warning("fresh UUID: "+uuid)
		return uuid
	
	func proxy()->ProjectFileUUIDProxy:
		return ProjectFileUUIDProxy.new(filedb, get_uuid())
	
	func pathproxy()->ProjectFileProxy:
		return super.proxy()
	
	## Write a string to this file, replacing its original contents
	## <br>In actuality, it writes a JSON document containing the uuid & the contents you pass
	func write(contents:String)->bool:
		if internal_file:
			return false

		
		var raw:String = UUID_SNIPPET.replace('{uuid}', get_uuid()) + '\n' + contents
		_contents = contents
		
		# old json-based system ↓
		#var raw := JSON.stringify({
			#"uuid": get_uuid(),
			#"contents": contents
		#}, "  ")
		
		self.hash = filedb.hashthis(raw) ## FIXME: why is this hashing the data including the UUID metadata?
		if filedb._write_to(path, raw):
			filedb.modified.emit(self)
			filedb.wobbled.emit(self)
			return true
		return false


	## Return the file contents as string, uses a cache by default
	## <br>Abstracts away the UUID json wrapper
	func read(use_cache:=false)->String:
		if use_cache and _contents != null:
			return _contents
		
		var raw:String = FileUtils.read(get_absolute_path())
		self.hash = filedb.hashthis(raw)
		if internal_file:
			return raw
		
		var found_uuid:String = raw.split('\n', true, 1)[0] # grab first line
		if UUID_END in found_uuid and UUID_START in found_uuid:
			found_uuid = found_uuid.split(UUID_START, true, 1)[1] # grab everything after UUID_START
			found_uuid = found_uuid.split(UUID_END, true, 1)[0] # grab everything before UUID_END
			found_uuid = found_uuid.strip_edges()
		else:
			found_uuid = ""
		
		if found_uuid == "":
			# File is a .script, but is plain text isntead of proper JSON UUID wrapper
			Logger.warning('File '+path+' is malformed!')
			Logger.debugE(raw)
			# Invent a new UUID
			if uuid == null:
				uuid = filedb._fresh_uuid()
				Logger.warning(str(self)+" UUID FRESH: "+uuid)
			_contents = raw
			# Write it to system immediatly to set the UUID in stone
			write(_contents)
		else:
			uuid = found_uuid
			#Logger.debug(str(self)+" UUID FROM READ: "+uuid)
			_contents = raw.split('\n', true, 1)[1] # grab everything after the first line (which is the UUID)
		return _contents

class ProjectFileProxy extends RefCounted:
	## Emitted when the file ath the path on disk has been changed
	signal modified()
	## Emitted when the file at the path is deleted
	signal deleted()
	## Emitted when the file at the path is new
	signal created()
	## Emitted when the file at the path has been moved to another path. 
	## Set `follow_moves` to `true` to make this ProjectFileProxy instance automatically switch to the new path
	signal moved(old_path, new_path)
	signal wobbled()
	## Automatically changes own path to the new path of the file when its moved
	var follow_moves:bool = false
	## Path this proxy looks at
	var path:String:
		get = get_local_path
	var filedb:FileDB
	
	func _to_string():
		return "<PROXY;"+path+';'+str(get_instance_id())+'>'
	
	func _init(filedb:FileDB, path:String):
		self.filedb = filedb
		self.path = path

		filedb.created.connect(func (file): if file.path == path: created.emit())
		filedb.wobbled.connect(func (file): if file.path == path: wobbled.emit())
		filedb.modified.connect(func (file): if file.path == path: modified.emit())
		filedb.deleted.connect(func (file): if file.path == path: deleted.emit())
		filedb.moved.connect(func (file, old_path):
			if old_path == path:
				if follow_moves:
					path = file.path
				moved.emit(old_path, file.path)
			)
	func get_local_path()->String:
		return path
	
	func proxy()->FileDB.ProjectFileProxy:
		return solidify().proxy()
	
	func solidify()->FileDB.ProjectFile:
		return filedb.get_file(path)

class ProjectFileUUIDProxy extends ProjectFileProxy:
	var uuid:String
	func _init(filedb:FileDB, uuid:String):
		self.filedb = filedb
		self.uuid = uuid
		filedb.created.connect(func (file): if file is ProjectFileUUID and file.uuid == uuid: created.emit())
		filedb.wobbled.connect(func (file): if file is ProjectFileUUID and file.uuid == uuid: wobbled.emit())
		filedb.modified.connect(func (file): if file is ProjectFileUUID and file.uuid == uuid: modified.emit())
		filedb.deleted.connect(func (file): if file is ProjectFileUUID and file.uuid == uuid: deleted.emit())
		filedb.moved.connect(func (file, old_path):
			if file is ProjectFileUUID and file.uuid == uuid:
				moved.emit(old_path, file.path)
			)
	
	func solidify()->FileDB.ProjectFileUUID:
		return filedb.get_file_by_uuid(uuid)

	func get_local_path()->String:
		return solidify().get_local_path()
	
	func get_uuid():
		return uuid

	func _to_string():
		return "<PROXY;"+uuid+';'+str(get_instance_id())+'>'

## Figures out what FILETYPE a given path is. Accepts only absolute paths
func identify(path:String, check_filesystem:bool = true)->FILETYPE:
	var filename = path.get_file()
	if filename.ends_with('/') or (check_filesystem and FileUtils.exists(path, true, false)):
		return FILETYPE.DIRECTORY
	elif check_filesystem and not FileUtils.exists(path, false, true):
		return FILETYPE.INVALID
	var extension = '.'+path.get_extension().to_lower()
	
	for filetype in file_associations:
		if extension in file_associations[filetype]:
			return filetype
	return FILETYPE.UNKNOWN

func _ready():
	clean_index.call_deferred()

## Return a list of all indexed filenames, relative to the project directory
func get_filenames()->PackedStringArray:
	return files.keys()

## Returns all indexed files. be careful with this one
func get_files()->Dictionary:
	return files.duplicate()

## Get a proxy for all indexed files. better to use get_proxy for specific files
func get_proxies()->Dictionary:
	var out := {}
	for key in files:
		out[key] = files[key].proxy()
	return out

## Get a specific ProjectFile object by project-local path. You should prefer get_proxy to avoid stale references
func get_file(path:String)->ProjectFile:
	if path.ends_with('/'):
		path = path.substr(0, len(path)-1)
#	Logger.debug(path+": "+str(files.get(path)))
	return files.get(path)

## Get a specific >ProjectFileUUID object by uuid.
func get_file_by_uuid(uuid:String)->ProjectFileUUID:
	return uuids.get(uuid)

## Get a proxy of a specific file by project-local path. Shorthand for get_file(path).proxy()
func get_proxy(path:String)->ProjectFileProxy:
	var file = get_file(path)
	assert(file is ProjectFile)
	return file.proxy()

func get_proxies_in_directory(directory:String)->Array[ProjectFileProxy]:
	var out:Array[ProjectFileProxy] = []
	## If path begins_with res:// all good else add project path !!
	var made_project_relative := false
	if not directory.begins_with('res://'):
		directory = main.project.path + directory
		made_project_relative = true
	
	for path in FileUtils.walk_flat(directory):
		if path.begins_with(directory):
			if made_project_relative:
				path = path.substr(len(main.project.path))
			out.append(get_proxy(path))
	return out

func get_files_in_directory(directory:String)->Array[ProjectFile]:
	var out:Array[ProjectFile] = []
	var made_project_relative := false
	if not directory.begins_with('res://'):
		directory = main.project.path + directory
		made_project_relative = true
	
	for path in FileUtils.walk_flat(directory):
		if path.begins_with(directory):
			if made_project_relative:
				path = path.substr(len(main.project.path))
			out.append(get_file(path))
	return out

func _write_to(path:String, text:String)->bool:
	return FileUtils.write(main.project.path + path, text)

## Use ProjectFile.write(contents) instead
func write_file(path:String, content:String)->bool:
	var file = get_file(path)
	if file == null:
		if FileUtils.exists(file.get_absolute_path(), false, true):
			Logger.warning("File '"+path+"' exists on disk but is unknown to us, ingesting now.")
			file = _makefile(path)
			_write_to(path, content)
			ingest(file)
			modified.emit(file)
			wobbled.emit(file)
			return true
		else:
			Logger.info("File '"+path+"' didn't exist, making now")
			create_file(path, content)
			return true
	if _write_to(path, content):
		modified.emit(file)
		wobbled.emit(file)
		return true
	return false

## Create a new file & write into it. Accepts only project-relative paths
## [br]Returns either ProjectFileProxy or ProjectFileUUIDProxy
func create_file(path:String, contents:String="")->ProjectFileProxy:
	if path in files:
		Logger.error('File "'+path+'" already exists')
		return null

	var new := _makefile(path, false)
	if new.write(contents):
		ingest(new)
		Logger.debug("Created file "+path)
		created.emit(new)
		wobbled.emit(new)
		return new.proxy()
	else:
		Logger.error("Could not write file "+main.project.path + path)
	return null

## Creates new directories recursively. Only accepts project-relative paths
func create_directory(path:String):
	if path in files:
		Logger.error('directory "'+path+'" already exists')
		return false
	var out := DirAccess.make_dir_recursive_absolute(path)
	if out == OK:
		var new := _makefile(path)
		ingest(new)
		created.emit(new)
	else:
		Logger.error("Could not create directory "+path)
	return out

## Index all project files. voids old ProjectFile instances. prefer 
func clean_index()->void:
	files = {}
	Logger.debug("FileDB clean index start")
	for path in main.project.get_files():
		ingest(_makefile(path))
	Logger.debug("FileDB clean index complete")
	Logger.debug()
	clean_indexed.emit()

## Indexes all project files, but compares the differences with the files in memory in case stuff got moved
## <br>Attempts to keep as many ProjectFiles in memory as-is
func index():
	var oldfiles = files
	var olduuids = uuids
	var oldhashes := {}
	
	var newfiles := {}
	var newuuids := {}

	var move_emitters := {}
	var create_emitters := []
	var delete_emitters := []
	var modify_emitters := []
	
	uuids = {}
	files = {}
	Logger.info("Re-indexing project...")
	
	var __files = main.project.get_files()
	for path in __files:
		# Quickly create a ProjectFile for each item
		# These are used for comparisons
		# Only if the file is completely new should these objects get ingested
		# Otherwise, in the case of moving/renaming/editing of a known file, re-ingest that instead
		var newfile = _makefile(path)
		newfiles[newfile.path] = newfile
		if newfile is ProjectFileUUID:
			if uuids.has(newfile.get_uuid()):
				Logger.error(str(newfile)+'s UUID '+newfile.get_uuid()+' is already in use by '+str(uuids[newfile.get_uuid()])+'!!!')
				newfile.uuid = _fresh_uuid()
				newfile.write(newfile.read())
			uuids[newfile.get_uuid()] = newfile
		else:
			# Used for best-effort matching of new assets to old ones
			# Breaks if the asset was not only moved, but also had its contents modified
			# Currently only scripts & scenes can use UUIDs 
			oldhashes[newfile.get_hash()] = newfile

	for _path in __files:
		# Loop over all the files in the latest snapshot
		# Compare it to the existing files we had in memoy
		# Issue the correct signals
		var path:String = _path
		if path.ends_with('/'):
			path = path.substr(0, len(path)-1)
		var newfile:ProjectFile = newfiles[path]
		Logger.debugP(str(newfile))
		
		if newfile.is_directory():
			if oldfiles.has(newfile.path):
				ingest(oldfiles[newfile.path])
				#Logger.debugE('OLD')
			else:
				ingest(newfile)
				#Logger.debugE('NEW')
				create_emitters.append(newfile)
				#created.emit(newfile)
			#Logger.debugE('directory :: no need for content comparisons')
			continue
		
		if oldfiles.has(path):
			Logger.debugE("Path known")
			var oldfile = oldfiles[path]
			if newfile is ProjectFileUUID:
				Logger.debugE("Has UUID")
				if newfile.get_uuid() == oldfile.get_uuid():
					# File is a scene/script, ingested before, path not changed
					ingest(oldfile)
					Logger.debugE("UUID matches")
					if oldfile.get_hash() != newfile.get_hash():
						# File has been modified since last read
						Logger.debugE("modified")
						modify_emitters.append(oldfile)
						#modified.emit(oldfile)
				else:
					Logger.debugE("UUID does NOT match: "+oldfile.get_uuid()+' vs '+newfile.get_uuid())
					# Old file got replaced by a completely different file!!
					if not newuuids.has(oldfile.get_uuid()):
						# Old file got deleted! rip 😔
						#Logger.debugE("deleted: "+str(oldfile))
						#delete_emitters.append(oldfile)
						#deleted.emit(oldfile)
						pass
					else:
						# the old file still exists somewhere!
						# (will get picked up in another loop)
						pass
					if olduuids.has(newfile.get_uuid()):
						# This new file was moved here from another known place!!
						var new_old:ProjectFileUUID = olduuids[newfile.get_uuid()]
						var oldpath := new_old.path
						new_old.path = path
						#ingest(new_old)
						Logger.debugE("Moved from "+oldpath)
						move_emitters[oldpath] = move_emitters.get(oldpath, []) + [new_old]
						#moved.emit(new_old, oldpath)
					else:
						# This new file was created externally! scary!
						Logger.debugE("Entirely new")
						ingest(newfile)
						create_emitters.append(newfile)
						#created.emit(newfile)
			else:
				Logger.debugE("No UUID")
				# File has no uuid: not a script/scene.
				ingest(oldfile)
				if oldfile.get_hash() != newfile.get_hash():
					# This file was edited!
					Logger.debugE("Hashes dont match: "+oldfile.get_hash()+' vs '+newfile.get_hash())
					modify_emitters.append(oldfile)
					#modified.emit(oldfile)
				else:
					Logger.debugE("Hashes match")
					# This is the exact same file. untouched!
					pass
		else:
			# File is new OR was moved here without replacing an existing file
			Logger.debugE("new or moved")
			if newfile is ProjectFileUUID:
				# File is a new or moved script!
				Logger.debugE("has UUID")
				if olduuids.has(newfile.get_uuid()):
					# This new file was moved here from another known place!!
					var new_old:ProjectFileUUID = olduuids[newfile.get_uuid()]
					var oldpath := new_old.path
					Logger.debugE("moved from "+oldpath)
					new_old.path = path
					#ingest(new_old)
					move_emitters[oldpath] = move_emitters.get(oldpath, []) + [new_old]
					#moved.emit(new_old, oldpath)
				else:
					# This new file was created externally! scary!
					Logger.debugE("Fully new")
					ingest(newfile)
					create_emitters.append(newfile)
					#created.emit(newfile)
			else:
				# File is a new or moved asset!
				Logger.debugE("No UUID")
				if oldhashes.has(newfile.get_hash()):
					# File got moved from somewhere else!!
					var oldfile:ProjectFile = oldhashes[newfile.get_hash()]
					var oldpath := oldfile.path
					Logger.debugE("Moved from "+oldpath)
					oldfile.path = path
					#ingest(oldfile)
					move_emitters[oldpath] = move_emitters.get(oldpath, []) + [oldfile]
					#moved.emit(oldfile, oldpath)
				else:
					# File *just* got created, OR it was both moved & modified!! scary!!
					Logger.debugE("Fully new")
					ingest(newfile)
					create_emitters.append(newfile)
					#created.emit(newfile)
	var newvals = files.values()
	for oldfile in oldfiles.values():
		if oldfile not in newvals:
			deleted.emit(oldfile)
	for oldpath in move_emitters:
		var items = move_emitters[oldpath]
		var f = items.pop_back()
		ingest(f)
		moved.emit(f, oldpath)
		wobbled.emit(f, oldpath)
		if items:
			for item in items:
				ingest(item)
			create_emitters = create_emitters + items
	for deleter in delete_emitters:
		deleted.emit(deleter)
	for modif in modify_emitters:
		modified.emit(modif)
		wobbled.emit(modif)
	for create in create_emitters:
		created.emit(create)
		wobbled.emit(create)

	Logger.debug("FileDB re-index complete")
	indexed.emit()

## Enter the ProjectFile into the FileDB
func ingest(file:ProjectFile)->ProjectFileProxy:
	assert(file != null)
	file.read()
	assert(file.path is String)
	files[file.path] = file
	assert(files.get(file.path) == file)
	if file is ProjectFileUUID:
		uuids[file.get_uuid()] = file
	Logger.debugP("Ingested file "+str(file))

	return file.proxy()

## Return md5 hash of the given string
func hashthis(contents:String)->String:
	return contents.md5_text()

func _fresh_uuid()->String:
	var out:String = UUIDGEN.v4()
	while out in uuids:
		out = UUIDGEN.v4()
	return out

## Initialises a ProjectFile. Accepts project-relative paths.
func _makefile(path:String, check_filesystem:bool=true)->ProjectFile:
	# Makes a ProjectFile instance
	# does NOT write anything to the filesystem!
	# if you want that, look at create_file()
	var internal:bool = path.begins_with('res://')
	var type:FILETYPE
	if not internal:
		type = identify(main.project.path + path, check_filesystem)
	else:
		type = identify(path, check_filesystem)
		
	if path.ends_with('/'):
		path = path.substr(0, len(path)-1)
	#Logger.debugP(path)
	#Logger.debugE(main.project.path + path)
	#Logger.debugE(FILETYPE.keys()[type])
	var hidden_file:bool = not path.begins_with('source') and not path.begins_with('misc') and not path.begins_with(Constants.EngineLuaDirectory)

	if (type == FILETYPE.SCRIPT or type == FILETYPE.SCENE):
		return ProjectFileUUID.new(self, path, type, hidden_file, internal, null) 
	else:
		return ProjectFile.new(self, path, type, hidden_file, internal)

func _notification(what):
	if what == NOTIFICATION_APPLICATION_FOCUS_IN:
		index()
		# TODO: check for changed files
