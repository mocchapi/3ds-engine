extends Node
class_name PipelineDB
## Cache of available pipeline types, and user-configured pipelines

signal type_registered(type:PipelineType)
signal config_registered(config:PipelineConfig)

@onready var filedb:FileDB = $/root/main/services/FileDB

var types:Dictionary = {}		## typename: PipelineType
var configs:Dictionary = {}		## uuid: PipelineConfig

func register_type(type:PipelineType):
	if not type is PipelineType:
		Logger.warning('Cannot add PipelineType {0}, it is not a PipelineType class')
		return
	elif types.has(type.name):
		Logger.warning('Cannot add PipelineType "{0}" ({1}), a type of that name is already registered ({2})'.format([type.name, str(type), str(types[type.name])]))
		return
	types[type.name] = type
	Logger.info("Registered PipelineType {0}".format([type.name]))
	type_registered.emit(type)

func register_config(config:PipelineConfig):
	if configs.has(config.uuid):
		if config == configs[config.uuid]:
			Logger.warningP('PipelineConfig '+config.name+' @ '+config.proxy.path+' is already registered!')
		else:
			Logger.errorP('PipelineConfig '+config.name+' @ '+config.proxy.path+' has the same UUID as '+configs[config.uuid].name+' @ '+configs[config.uuid].proxy.path)
		return
	if not types.has(config.type):
		Logger.warningP('PipelineConfig '+config.name+' @ '+config.proxy.path+' is for unknown PipelineType '+config.type)
	Logger.info('Registered PipelineConfig {0} ({1})'.format([config.name, config.type]))
	configs[config.uuid] = config
	config_registered.emit(config)

func create_config(name:String, type:String, parameters:Dictionary={}):
	var config = PipelineConfig.new()
	config.uuid = UUIDGEN.v4()
	config.name = name
	config.type = type
	config.parameters = parameters.duplicate(true)
	filedb.create_file('pipelines/'+config.uuid+'.json', config.to_json())

func get_types()->Array[PipelineType]:
	return types.values()

func get_type(name:String):
	return types.get(name)

func get_configs()->Array[PipelineConfig]:
	return configs.values()

func get_config(uuid:String):
	return configs.get(uuid)

func _ready():
	register_type(preload("res://src/engine/pipelines/LPP-3DS/type.tres"))
	register_type(preload("res://src/engine/pipelines/Dummy/type.tres"))

	await filedb.clean_indexed
	for file in filedb.get_proxies_in_directory('pipelines/'):
		register_config(_make_config(file))


func _make_config(proxy:FileDB.ProjectFileProxy)->PipelineConfig:
	var f = proxy.solidify().read()
	var json = JSON.parse_string(f)
	if json == null:
		Logger.errorP("Cannot create PipelineConfig: "+proxy.path+' is not valid JSON!')
	
	var out := PipelineConfig.new()
	out.proxy = proxy.proxy()
	out.proxy.follow_moves = true
	out.name = json['name']
	out.type = json['type']
	out.uuid = json['uuid']
	out.parameters = json['parameters']
	return out

func _on_file_db_created(file:FileDB.ProjectFile):
	if file.path.begins_with('pipelines/'):
		register_config(_make_config(file.proxy()))
