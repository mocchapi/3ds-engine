extends Node
class_name Main
@onready var filedb:FileDB = %FileDB
@onready var luaserv:LuaServ = %LuaServ
@onready var scriptserv:ScriptServ = %ScriptServ


@export var project:Project

@onready var interfaces = %main_interfaces
@onready var scenes = interfaces.get_node("Scenes")
@onready var scripts = interfaces.get_node("Scripts")
@onready var docs = interfaces.get_node("Docs")
@onready var builds = interfaces.get_node("Builds")
@onready var debugger = interfaces.get_node("Debugger")

signal save_started()
signal save_completed()

var is_saving:bool = false
## How many times _proclaim_save_completed has been called
var _save_reports:int = 0
## How many nodes are **supposed** to call _proclaim_save_completed()
var _savers:int = 0
## Add the "project_savers" group to all nodes that have to do business when saving the project.
## They will get their "project_save_triggered" function called with a callable as argument
## When they are done saving, they must call this callable with `self`. This callable may only be called ONCE or everything breaks.
""" <some node in the "project_save_triggered" group>

func project_save_triggered(done):
	do_some_saving()
	await do_more_savin	g()
	
	done.call(self)
"""

func _ready():
	Logger.debug("Entered editing environment for project "+project.config.info_name)

func _process(delta):
	pass


func save():
	if is_saving:
		Logger.error("Ignoring project save: a save is already in progress")
		return
	
	_save_reports = 0
	_savers = len(get_tree().get_nodes_in_group("project_savers"))
	Logger.line()
	Logger.info("Saving "+str(_savers)+" nodes...")
	
	save_started.emit()
	get_tree().call_group_flags(
		SceneTree.GROUP_CALL_DEFERRED,
		"project_savers",
		"project_save_triggered",
		_proclaim_save_completed
	)
	
	await save_completed

func _proclaim_save_completed(node, actually_saved:=true):
	_save_reports += 1
	if actually_saved:
		Logger.debug( (str(node)) +" saved! " + str(_save_reports) + '/' + str(_savers) )
	else:
		Logger.debug( (str(node)) +" did not require saving! " + str(_save_reports) + '/' + str(_savers) )
	if _save_reports == _savers:
		Logger.info("Saving complete!")
		Logger.line()
		is_saving = false
		save_completed.emit()


func _on_file_db_created(file):
	Logger.warning('CREATED: '+file.path)
	pass # Replace with function body.


func _on_file_db_deleted(file):
	Logger.warning('DELETED: '+file.path)


func _on_file_db_modified(file):
	Logger.warning('EDITED: '+ file.path)


func _on_file_db_moved(file, old_path):
	Logger.warning('MOVED: '+old_path +' -> '+ file.path)


func _on_file_db_wobbled(file):
	Logger.warning("WOBBLED: "+file.path)
