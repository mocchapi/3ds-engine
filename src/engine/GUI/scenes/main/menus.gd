extends MenuBar

@onready var main:Main = $/root/main

@onready var project = $Project
#@onready var editor = $Editor
@onready var help = $Help


# Called when the node enters the scene tree for the first time.
func _ready():
	project.set_item_text(0, $"/root/main".project.config.info_name)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_project_id_pressed(id):
	match id:
		3: ## Save project
			main.save()
		1: ## Reload
			pass
		2: ## Return to project list
			main.save()
			Switcher.project_manager()


func _on_editor_id_pressed(id):
	pass # Replace with function body.


func _on_help_id_pressed(id):
	pass # Replace with function body.
