extends TabContainer

func show_interface(interface_name:String):
	var node = get_node(interface_name)
	if node != null:
		current_tab = node.get_index()

func show_scenes():
	show_interface("Scenes")

func show_scripts():
	show_interface("Scripts")

func show_docs():
	show_interface("Docs")

func show_builds():
	show_interface("Builds")

func show_debugger():
	show_interface("Debugger")
