extends MarginContainer

@onready var pipelinedb:PipelineDB = $/root/main/services/PipelineDB

@onready var pipeline_buttons_list = %pipeline_buttons_list
@onready var btn_add_pipeline:MenuButton = %btn_add_pipeline
@onready var btn_remove_pipeline = %btn_remove_pipeline

@onready var tabs_pipeline_params = %tabs_pipeline_params
@onready var tabs_build_progress = %tabs_build_progress

const packedscene_button = preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/pipeline_item_button.tscn")
const packedscene_params = preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/pipeline_parameters.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	btn_add_pipeline.get_popup().index_pressed.connect(_on_btn_add_pipeline_pressed)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_pipeline_db_type_registered(type:PipelineType):
	if btn_add_pipeline == null:
		btn_add_pipeline = %btn_add_pipeline
	btn_add_pipeline.get_popup().add_icon_item(type.icon, type.name)
	var idx:int = btn_add_pipeline.get_popup().item_count-1
	btn_add_pipeline.get_popup().set_item_icon_max_width(idx, 48)
	btn_add_pipeline.get_popup().set_item_tooltip(idx, type.description)


func _on_btn_pipelines_popout_pressed():
	get_parent().pop()


func _on_pipeline_db_config_registered(config:PipelineConfig):
	var instance = packedscene_button.instantiate()
	instance.config = config
	pipeline_buttons_list.add_child(instance)
	instance.pressed.connect(func (): _on_pipeline_btn_pressed(instance))
	if pipeline_buttons_list.get_child_count() == 1:
		_on_pipeline_btn_pressed(instance)

	var param_scene = packedscene_params.instantiate()
	param_scene.config = config
	tabs_pipeline_params.add_child(param_scene)

func _on_pipeline_btn_pressed(button):
	var idx:int = 0
	var i:int = 0
	for btn in pipeline_buttons_list.get_children():
		btn.is_selected = btn == button
		if btn == button:
			idx = i
		i += 1
	tabs_pipeline_params.current_tab = idx
	tabs_build_progress.current_tab = idx

func _on_btn_add_pipeline_pressed(index:int):
	var type_name:String = btn_add_pipeline.get_popup().get_item_text(index)
	pipelinedb.create_config('New pipeline', type_name)


func _on_btn_progress_popout_pressed():
	var node = tabs_build_progress.get_current_tab_control()
