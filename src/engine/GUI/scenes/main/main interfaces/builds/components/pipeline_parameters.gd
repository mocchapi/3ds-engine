extends VBoxContainer

@onready var pipelinedb:PipelineDB = $/root/main/services/PipelineDB

@export var config:PipelineConfig

@onready var line_name = %line_name
@onready var parameters = %parameters
@onready var btn_start_build = %btn_start_build

@onready var panel_unknown_type = $panel_unknown_type
@onready var lbl_unknown_type = $panel_unknown_type/HBoxContainer/lbl_unknown_type

var type:PipelineType

var param_scenes = {
	PipelineTypeParameter.TYPE.string: preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/parameter types/string.tscn"),
	PipelineTypeParameter.TYPE.text: preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/parameter types/text.tscn"),
	PipelineTypeParameter.TYPE.boolean: preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/parameter types/bool.tscn"),
	PipelineTypeParameter.TYPE.integer: preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/parameter types/int.tscn"),
	PipelineTypeParameter.TYPE.float: preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/parameter types/float.tscn"),
	PipelineTypeParameter.TYPE.percentage: preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/parameter types/percentage.tscn"),
	PipelineTypeParameter.TYPE.choice: preload("res://src/engine/GUI/scenes/main/main interfaces/builds/components/parameter types/choice.tscn"),
}

# Called when the node enters the scene tree for the first time.
func _ready():
	line_name.text = config.name
	type = pipelinedb.get_type(config.type)
	lbl_unknown_type.text = lbl_unknown_type.text.format([config.name])
	if type == null:
		panel_unknown_type.show()
		return
	$PanelContainer/HBoxContainer/img_type.texture = type.icon
	
	_build_parameter_list()
	_populate_parameter_list()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _build_parameter_list():
	for item in type.parameters:

		var instance = param_scenes.get(
				item.type, param_scenes[PipelineTypeParameter.TYPE.string]
			).instantiate()
		instance.config = item
		parameters.add_child(instance)
		

func _populate_parameter_list():
	pass

func _on_line_name_text_changed(new_text):
	config.name = new_text
