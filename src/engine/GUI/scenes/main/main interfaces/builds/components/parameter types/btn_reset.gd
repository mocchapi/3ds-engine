extends Button

@export var root:Node

func _pressed():
	if root != null:
		root.reset()
