@tool
extends PanelContainer

@export var section_name:String = "Section":
	set = set_section_name
@export var done:bool = false:
	set = set_done
@export var success:bool = true:
	set = set_success
@export var minimized:bool = false:
	set = set_minimized
@export var progress:float = 0.0:
	set = set_progress
@export var type:PipelineBuilder.SECTION_TYPE = PipelineBuilder.SECTION_TYPE.simple:
	set = set_type
@export_multiline var log:String:
	get = get_log, set = set_log
@export var max_log_height:float = 215.0
var elapsed_time:float = 0.0

@onready var lbl_time = $main/top/lbl_time
@onready var lbl_name = $main/top/lbl_name
@onready var lbl_log = $main/panel_log/scroll/lbl_log
@onready var panel_log = $main/panel_log

func _ready():
	set_type(type)
	set_done(done)
	set_log(log)
	set_minimized(minimized)
	set_success(success)

func set_done(new:bool):
	done = new
	set_process(not new)
	theme_type_variation = &"PanelContainerGreenMargin" if done else &"PanelContainerPinkMargin"
	$main/tabs_types/type_counter/hbox/icon.visible = not done
	$main/tabs_types/type_simple.visible = not done and type == PipelineBuilder.SECTION_TYPE.simple

func _process(delta):
	if done:
		set_process(false)
	else:
		if lbl_time == null:
			lbl_time = $main/top/lbl_time
		elapsed_time += delta
		var t:Dictionary = Time.get_time_dict_from_unix_time(elapsed_time)
		lbl_time.text = str(t['hour']*60 + t['minute']).pad_zeros(2) + ':' + str(t['second']).pad_zeros(2)

func set_success(new:bool):
	success = new

func set_minimized(new:bool):
	minimized = new

func set_type(new:PipelineBuilder.SECTION_TYPE):
	type = new
	$main/tabs_types/type_counter.visible = new == PipelineBuilder.SECTION_TYPE.counter
	$main/tabs_types/type_simple.visible = new == PipelineBuilder.SECTION_TYPE.simple
	$main/tabs_types/type_progress.visible = new == PipelineBuilder.SECTION_TYPE.progressbar
	set_progress(progress)

func set_log(new:String):
	if lbl_log == null:
		lbl_log = $main/panel_log/scroll/lbl_log
	if panel_log == null:
		panel_log = $main/panel_log
	if lbl_log == null or panel_log == null:
		return
	lbl_log.text = new
	panel_log.visible = new != ''
	if lbl_log.size.y < max_log_height:
		panel_log.custom_minimum_size.y = lbl_log.size.y
	else:
		panel_log.custom_minimum_size.y = max_log_height
	print()
	print(panel_log.custom_minimum_size)
	print(lbl_log.size.y)
	print(max_log_height)
	

func get_log()->String:
	if lbl_log == null:
		if not Engine.is_editor_hint():
			return ""
		lbl_log = $main/panel_log/scroll/lbl_log
	return lbl_log.text

func set_progress(new:float):
	progress = new
  
	match type:
		PipelineBuilder.SECTION_TYPE.counter:
			$main/tabs_types/type_counter/hbox/lbl_count.text = str(new)
		PipelineBuilder.SECTION_TYPE.progressbar:
			$main/tabs_types/type_progress.value = new*100

func set_section_name(new:String):
	pass
