extends BoxContainer
@export var type:PipelineTypeParameter.TYPE
@export var value_target:Node

@export var label:Control
@export var tooltips:Array[Control]

var config:PipelineTypeParameter

func _ready():
	build()

func _set_value(value):

	match type:
		PipelineTypeParameter.TYPE.string, PipelineTypeParameter.TYPE.text, \
		PipelineTypeParameter.TYPE.system_path, PipelineTypeParameter.TYPE.project_path:
			value_target.text = '' if value == null else str(value)
		PipelineTypeParameter.TYPE.boolean:
			if typeof(value) in [TYPE_INT, TYPE_FLOAT, TYPE_BOOL]:
				value = bool(value)
			elif value == null:
				value = false
			else:
				value = bool(len(value))
			value_target.button_pressed = value
		PipelineTypeParameter.TYPE.integer:
			value_target.value = int(value) if value != null else 0
		PipelineTypeParameter.TYPE.float:
			value_target.value = float(value) if value != null else 0.0
		PipelineTypeParameter.TYPE.percentage:
			value_target.value = clamp(float(value) if value != null else 0.0, 0.0, 1.0)
		PipelineTypeParameter.TYPE.choice:
			for idx in value_target.item_count:
				if value_target.get_item_metadata(idx) == value:
					value_target.select(idx)
					return
		_:
			assert(false, "Implement `_set_value`")

func _get_value():
	match type:
			PipelineTypeParameter.TYPE.string, PipelineTypeParameter.TYPE.text, \
			PipelineTypeParameter.TYPE.system_path, PipelineTypeParameter.TYPE.project_path:
				return value_target.text
			PipelineTypeParameter.TYPE.boolean:
				return value_target.button_pressed
			PipelineTypeParameter.TYPE.integer:
				return int(value_target.value)
			PipelineTypeParameter.TYPE.float:
				return float(value_target.value)
			PipelineTypeParameter.TYPE.percentage:
				return clamp(value_target.value, 0.0, 1.0)
			PipelineTypeParameter.TYPE.choice:
				return value_target.get_selected_metadata()
			_:
				assert(false, "`_get_value` is not implemented")

func _set_label():
	if label == null:
		label = find_child('Label')
	label.text = config.name

func build():
	assert(type == config.type or type == PipelineTypeParameter.TYPE.string)
	_set_label()
	
	if config.tooltip != '':
		if tooltips.is_empty():
			tooltips = [label, value_target]
		for item in tooltips:
			item.tooltip_text = config.tooltip
	
	if type == PipelineTypeParameter.TYPE.choice:
		for item in config.choices:
			value_target.add_item(str(item) if item != null else '')
			value_target.set_item_metadata(value_target.item_count-1, item)
	
	_set_value(config.get_default())

func reset():
	_set_value(config.get_default())

func populate(value):
	_set_value(value)

func value():
	return _get_value()
