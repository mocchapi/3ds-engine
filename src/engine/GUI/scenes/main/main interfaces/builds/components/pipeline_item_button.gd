@tool
extends MarginContainer

signal pressed

@export var config:PipelineConfig

@export var is_selected:bool = false:
	set = set_is_selected
@export var is_building:bool = false:
	set = set_is_building

@onready var img_icon = %img_icon
@onready var lbl_name = %lbl_name
@onready var lbl_type = %lbl_type

func _ready():
	config.name_changed.connect(set_pipeline_name)
	lbl_type.text = config.type
	set_pipeline_name(config.name)
	var type = $/root/main/services/PipelineDB.get_type(config.type)
	var icon = type.icon if type != null else preload("res://icon.svg")
	img_icon.texture = icon
	$Button.pressed.connect( func (): pressed.emit() )

func set_pipeline_name(name:String):
	lbl_name.text = name

func set_is_building(new:bool):
	is_building = new
	$gui/VBoxContainer/building.visible = new

func set_is_selected(new:bool):
	is_selected = new
	$bg_if_selected.visible = new
