extends Label


func _ready():
	visibility_changed.connect(func (): self.set_process( self.is_visible_in_tree() ))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	text = str(snapped(get_parent().value*100, 0.1)).pad_decimals(1) + '%'
