extends "res://src/engine/GUI/scenes/main/main interfaces/builds/components/parameter types/basetype.gd"


func _set_value(value):
	if value == null:
		value = ''
	find_child('value').text = str(value)

func _get_value():
	return find_child('value').text
