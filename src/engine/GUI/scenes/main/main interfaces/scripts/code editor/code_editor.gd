extends PanelContainer

@onready var code_editor = $vbox/code_bg/code_editor
@onready var lbl_error = $vbox/PanelContainer/error_bar/lbl_error
@onready var error_bar = $vbox/PanelContainer
@onready var debounce = $debounce
@onready var lbl_error_line = $vbox/PanelContainer/error_bar/lbl_error_line

@onready var main = $/root/main

const CODE_HIGHLIGHT = preload("res://src/engine/assets/misc/code_highlight.tres")

var file:FileDB.ProjectFileUUIDProxy

var scriptserv:ScriptServ
var save_pending:bool = false
var inspection_result:
	set(new):
		if code_editor:
			for i in code_editor.text.count('\n')+1:
				code_editor.set_line_background_color(i, Color(0,0,0,0))
		if new is ScriptServ.InspectionError and lbl_error:
			lbl_error.text = new.message
			lbl_error_line.text = 'Line ' + str(new.line)
			code_editor.set_line_background_color(new.line-1, Color.INDIAN_RED)
			error_bar.show()
		elif error_bar:
			error_bar.hide()
		#if inspection_result is LuaError and code_editor:
			#var line:int = int(inspection_result.message.split(':', true, 1)[0])-1
			#code_editor.set_line_background_color(line, )
		inspection_result = new

func _ready():
	code_editor.add_comment_delimiter('--', '\n', true)
	code_editor.delimiter_strings = Array(code_editor.delimiter_strings) + ['[[]]']
	code_editor.code_completion_prefixes = ['self.', 'self:', 'Engine.']
	
	scriptserv = main.scriptserv
	scriptserv.wobbled.connect(_on_scriptserv_wobbled)
	code_editor.syntax_highlighter = CODE_HIGHLIGHT.duplicate()
	
	file = file.proxy()
	file.follow_moves = true

	code_editor.text = file.solidify().read()
	## Disable input if its an internal engine file
	code_editor.editable = not file.solidify().internal_file
	code_editor.clear_undo_history()
	file.modified.connect(_on_file_modified)
	file.moved.connect(_on_file_moved)
	name = file.path
	
	inspection_result = scriptserv.get_inspection(file.get_uuid())


func _on_scriptserv_wobbled(wobbled_file:FileDB.ProjectFileUUID):
	if wobbled_file.get_uuid() == file.get_uuid():
		inspection_result = scriptserv.get_inspection(file.get_uuid())

func check_pending()->bool:
	save_pending = file.solidify().read() != code_editor.text
	return save_pending

func _on_file_modified():
	check_pending()
	Logger.debug("Save pending set to "+str(save_pending))

func _on_file_moved(new_path):
	name = file.path

func _on_code_editor_text_changed():
	debounce.start()

func project_save_triggered(done:Callable):
	if check_pending():
		save()
		done.call(self, true)
	else:
		done.call(self, false)

func save():
	file.solidify().write(code_editor.text)
	save_pending = false

func on_close_requested():
	# TODO: ask if to save or discard changes if modified
	if check_pending():
		save()
		Logger.info("Saved to "+file.path)
	get_parent().remove_child(self)
	queue_free()


func _on_debounce_timeout():
	Logger.debug("Debounce!")
	if check_pending():
		inspection_result = await scriptserv.inspect(code_editor.text, file.path, not file.solidify().internal_file)
	else:
		inspection_result = scriptserv.get_inspection(file.get_uuid())



func _on_code_editor_gui_input(event):
	if event.is_action_pressed("save"):
		main.save()
		get_viewport().set_input_as_handled()
	elif event.is_action("(code) duplicate"):
		code_editor.duplicate_lines()
		get_viewport().set_input_as_handled()
