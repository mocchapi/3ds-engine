extends VBoxContainer

const CODE_EDITOR = preload("res://src/engine/GUI/scenes/main/main interfaces/scripts/code editor/code_editor.tscn")

@onready var filedb:FileDB = %FileDB

@onready var tabs_open_scripts = $HBoxContainer/tabs_open_scripts
@onready var line_scriptstree_search = $HBoxContainer/PanelContainer/VBoxContainer/line_scriptstree_search
@onready var list_scripts = $HBoxContainer/PanelContainer/VBoxContainer/list_scripts


func open(path:String):
	var idx:=0
	for tab in tabs_open_scripts.get_children():
		if tab.file.path == path:
			tabs_open_scripts.current_tab = idx
			return
		idx += 1
	
	var file = filedb.get_proxy(path)
	if file.solidify().type == FileDB.FILETYPE.SCRIPT:
		Logger.debug(file)
		assert(file is FileDB.ProjectFileUUIDProxy)
	
	var instance = CODE_EDITOR.instantiate()
	instance.file = file
	tabs_open_scripts.add_child(instance)
	tabs_open_scripts.current_tab = idx
	tabs_open_scripts.set_tab_button_icon(idx, preload("res://src/engine/assets/textures/icons/cross.png"))
	tabs_open_scripts.set_tab_title(idx, path)
