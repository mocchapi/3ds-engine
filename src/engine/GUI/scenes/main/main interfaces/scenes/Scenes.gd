extends VBoxContainer

const SCENE_EDITOR = preload("res://src/engine/GUI/scenes/main/main interfaces/scenes/scene editor/scene_editor.tscn")

@onready var filedb:FileDB = %FileDB

@onready var tabs_open_scenes: TabContainer = $tabs_open_scenes


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func open(path:String):
	var idx:=0
	for tab in tabs_open_scenes.get_children():
		if tab.file.path == path:
			tabs_open_scenes.current_tab = idx
			return
		idx += 1
	
	var file = filedb.get_proxy(path)
	
	assert(file.solidify().type == FileDB.FILETYPE.SCENE)
	assert(file is FileDB.ProjectFileUUIDProxy)
	
	var instance = SCENE_EDITOR.instantiate()
	instance.file = file
	tabs_open_scenes.add_child(instance)
	tabs_open_scenes.current_tab = idx
	tabs_open_scenes.set_tab_button_icon(idx, preload("res://src/engine/assets/textures/icons/cross.png"))
	tabs_open_scenes.set_tab_title(idx, path)
