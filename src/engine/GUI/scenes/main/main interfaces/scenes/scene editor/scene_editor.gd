extends HSplitContainer

var file:FileDB.ProjectFileUUIDProxy

@onready var scene_data: EditorEditor = $scene_data

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	file = file.proxy()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass


func rebuild():
	for item in scene_data.get_children():
		scene_data.remove_child(item)
		item.queue_free()
	
	var json:String = file.solidify().read()
	var dict:Dictionary = JSON.parse_string(json)
	
	for item in dict['nodes']:
		pass


func serialize()->Dictionary:
	var serialized_nodes:Array[Dictionary] = []
	
	for node in scene_data.get_all_children():
		serialized_nodes.append(serialize_node(node))
	
	return {
		"meta": {
			"version": Constants.version,
		},
		"info": {
			"amount": len(serialized_nodes),
		},
		'nodes': serialized_nodes
		}

func serialize_node(node:scnNode)->Dictionary:
	return {
		path = node.get_local_path(),
		base_node = node.get,
		parameters = {
			
		}
	}
