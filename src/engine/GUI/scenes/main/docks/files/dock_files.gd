extends VSplitContainer

@onready var main:Main = $/root/main
@onready var filedb:FileDB = $/root/main/services/FileDB

@onready var line_search:LineEdit = %line_search
@onready var tree_files:Tree = %tree_files
@onready var popup_menu = $PopupMenu
@onready var popup_lineedit = $popup_lineedit
@onready var popup_new_script = $popup_new_script
@onready var popup_new_scene = $popup_new_scene



const icons = {
	FileDB.FILETYPE.SCRIPT: preload("res://src/engine/assets/textures/icons/filetypes/script.png"),
	FileDB.FILETYPE.SCENE: preload("res://src/engine/assets/textures/icons/filetypes/scene.png"),
	FileDB.FILETYPE.TEXTURE: preload("res://src/engine/assets/textures/icons/filetypes/scene.png"),
	FileDB.FILETYPE.AUDIO: preload("res://src/engine/assets/textures/icons/filetypes/audio.png"),
	FileDB.FILETYPE.DIRECTORY: preload("res://src/engine/assets/textures/icons/filetypes/directory.png"),
	FileDB.FILETYPE.UNKNOWN: preload("res://src/engine/assets/textures/icons/filetypes/unknown.png")	,
	FileDB.FILETYPE.INVALID: preload("res://src/engine/assets/textures/icons/filetypes/invalid.png"),
}

var leafs:Dictionary = {}
var root:TreeItem
var internal_root:TreeItem

func _ready():
	filedb.clean_indexed.connect(build_tree)
	filedb.created.connect(_on_filedb_created)

func _get_or_make_leaf(path:String, cache:Dictionary, root_item:TreeItem, tree:Tree)->TreeItem:
	var search = cache.get(path)
	if search != null:
		return search

	var split = Array(path.split('/'))
	if split[-1] == "":
		split.pop_back()
	var leaf_name = split.pop_back()
	var part_path = '/'.join(split)
	var p:TreeItem
	if len(split) > 0 and part_path != Constants.EngineLuaDirectory.substr(0,len(Constants.EngineLuaDirectory)-1):
		p = _get_or_make_leaf(part_path, cache, root_item, tree)
	else:
		if path.begins_with(Constants.EngineLuaDirectory):
			p = internal_root
		else:
			p = root_item

	var out:TreeItem= tree.create_item(p)
	out.set_text(0, leaf_name)

	var proxy = filedb.get_proxy(path)
	out.set_metadata(0, {'proxy':path})
	out.set_icon(0, icons.get(proxy.solidify().type))
	
	if proxy.path.begins_with(Constants.EngineLuaDirectory):
		out.set_custom_color(0, Color.DIM_GRAY)
		out.collapsed = true

	var tooltip = " "+leaf_name+"\n  "+proxy.solidify().get_friendly_type()+'\n  '+path
	out.set_tooltip_text(0,tooltip)
	cache[path] = out
	return out

func build_tree():
	Logger.debug("Start rebuild")
	
	leafs.clear()
	tree_files.clear()
	root = tree_files.create_item()
	internal_root = root.create_child()
	internal_root.set_text(0,"internal")
	
	for item in main.filedb.get_files().values():
		_on_filedb_created(item)
	
	internal_root.move_after(root.get_child(root.get_child_count()-1))

func _on_filedb_created(file:FileDB.ProjectFile):
	if not file.hidden_file:
		_get_or_make_leaf(file.path, leafs, root, tree_files)

func _on_tree_files_item_mouse_selected(position, mouse_button_index):
#	global_position + position
	if mouse_button_index == MOUSE_BUTTON_RIGHT:
		popup_menu.position = DisplayServer.mouse_get_position()
		popup_menu.popup()



# ↓ rightclick menu ↓


func _on_popup_menu_id_pressed(id:int):
	var leaf := tree_files.get_selected()
	var proxy:FileDB.ProjectFileProxy = filedb.get_proxy( leaf.get_metadata(0)['proxy'] )
	match id:
		9: # Open
			pass
		
		50: # New script
			Logger.debug("New script")
			popup_new_script.set_script_path(
					proxy.solidify().get_closest_directory(false) + 'New.script'
				)
			if proxy.solidify().type == FileDB.FILETYPE.SCRIPT:
				popup_new_script.set_script_extends(
					proxy.solidify().path
					)
			else:
				popup_new_script.set_script_extends("Node")
			popup_new_script.show()
		1: # New scene
			Logger.debug("New scene")
			var path = proxy.solidify().get_closest_directory(false) + 'New.scene'
			popup_new_scene.set_scene_path(path)
			popup_new_scene.show()
		2: # New Directory
			pass
		
		# 3 is a separator
		
		4: # Rename
			pass
		5: # Delete
			pass
		
		# 6 is a separator
		
		7: # Open in file explorer
			proxy.solidify().open_directory()
		8: # Open in external program
			proxy.solidify().open_external()
		9: # Open (internal)
			proxy.solidify().open_internal()


func _on_tree_files_item_activated():
	var leaf := tree_files.get_selected()
	if leaf == internal_root:
		leaf.collapsed = not leaf.collapsed
		return

	var proxy:FileDB.ProjectFileProxy = filedb.get_proxy( leaf.get_metadata(0)['proxy'] )
	if proxy.solidify().type == FileDB.FILETYPE.DIRECTORY:
		leaf.collapsed = not leaf.collapsed
	else:
		proxy.solidify().open_internal()


func _on_popup_lineedit_confirmed(text):
	pass # Replace with function body.
	
func new_script(script_path:String, script_extends:String):
	var proxy:FileDB.ProjectFileProxy = filedb.create_file(
		script_path, '{\n	extends "' + script_extends + '",\n	\n}')

func new_scene(scene_path:String, scene_root:String):
	var proxy:FileDB.ProjectFileProxy = filedb.create_file(
		scene_path, JSON.stringify( {'meta': {'version':Constants.version}, 'nodes': [] }, "	"))

func _on_popup_new_script_confirmed(script_path, script_extends):
	new_script(script_path, script_extends)
	


func _on_popup_new_scene_confirmed(scene_path: String, scene_root: String) -> void:
	new_scene(scene_path, scene_root)
