extends Node

# First scene thats loaded when the engine is launched
# Switches to project manager or directly to main or performs a build 
# Depending on specific commandline options

func _ready():
	Logger.info("Welcome to 3ds game engine (WIP)")
	Logger.info("This is version "+Constants.version)
	
	if OS.is_debug_build():
		Logger.info("This is a debug build")
	else:
		Logger.info("This is a release build. Some logging details will be unavailable")
	
	Logger.debug("commandline arguments are:")	
	Logger.startseg('CLI arguments')
	for item in OS.get_cmdline_args():
		Logger.debugP('"' +item + '"')
	Logger.endseg()
	
	Logger.line()
	Logger.empty()
	get_tree().change_scene_to_file.call_deferred("res://src/engine/GUI/scenes/projectmanager/projectmanager.tscn")
