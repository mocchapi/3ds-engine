extends PopupPanel

signal done(was_confirmed:bool, script_path:String, script_extends:String)
signal confirmed(script_path:String, script_extends:String)
signal cancelled()

@onready var opt_extends = %opt_extends
@onready var line_path = %line_path
@onready var lbl_error = %lbl_error
@onready var btns_cancel_confirm = $VBoxContainer/btns_cancel_confirm


func set_script_path(text:String):
	line_path.text = text
	var name_begin = text.rfind('/')
	var extension_begin = text.rfind('.')
	if ((name_begin+1 < extension_begin) or extension_begin == -1) and name_begin != -1:
		line_path.select(name_begin+1, extension_begin)
		line_path.caret_column = name_begin+1

func set_script_extends(value:String):
	for idx in opt_extends.item_count:
		if opt_extends.get_item_text(idx) == value:
			opt_extends.select(idx)
			return
	Logger.warning("No given unknown script to extend: " + value)
	opt_extends.select(0)

func _on_line_path_text_changed(new_text:String):
	btns_cancel_confirm.can_confirm = new_text.ends_with('.script')

func _on_btns_cancel_confirm_done(was_confirmed:bool):
	if was_confirmed:
		confirmed.emit(line_path.text, opt_extends.get_item_text(opt_extends.selected))
	else:
		cancelled.emit()
	hide()
	done.emit(was_confirmed, line_path.text, opt_extends.get_item_text(opt_extends.selected))




func _on_visibility_changed():
	if visible and line_path:
		line_path.grab_focus()


func _on_line_path_text_submitted(new_text):
	btns_cancel_confirm.if_allowed_confirm()
