extends MarginContainer
@onready var holder = $window/holder
@onready var btn_return = $btn_return
@onready var window = $window

func pop():
	to_window()

func to_window():
	for item in get_children():
		if item == window or item == btn_return:
			continue
		remove_child(item)
		holder.add_child(item)
	btn_return.show()
	window.size = size
	window.show()


func to_slot():
	for item in holder.get_children():
		holder.remove_child(item)
		add_child(item)
	btn_return.hide()
	window.hide()

func _ready():
	pass


func _on_btn_return_pressed():
	to_slot()


func _on_window_close_requested():
	to_slot()
