extends TabContainer

@export var button_icon:Texture = preload("res://src/engine/assets/textures/icons/cross.png")

func _ready():
	tab_changed.connect(__on_self_tab_changed)
	__on_self_tab_changed(current_tab)

func __on_self_tab_changed(new_tab:int):
	Logger.debug(str(new_tab) + ' :: ' + str(get_previous_tab()))
	set_tab_button_icon(get_previous_tab(), null)
	set_tab_button_icon(new_tab, button_icon)
