extends "res://src/engine/GUI/parts/buttonedtabcontainer/ButtonedTabContainer.gd"

@export var force_close:bool = false
@export var warn_on_missing_func:bool = true
var function_name:String = "on_close_requested"

func _ready():
	super()
	tab_button_pressed.connect(__on_self_btn_pressed)

func __on_self_btn_pressed(idx:int):
	var node = get_child(idx)
	if node != null:
		if node.has_method(function_name):
			node.on_close_requested()
			if force_close:
				Logger.debug("Forcibly removing "+str(node))
				remove_child(node)
				node.queue_free()
		else:
			remove_child(node)
			node.queue_free()
			if warn_on_missing_func:
				Logger.warning("Node "+str(node)+' has no method named "'+function_name+'"')
