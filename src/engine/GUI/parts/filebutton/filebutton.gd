extends Button

signal selected(path:String)

#● FILE_MODE_OPEN_FILE = 0
#The dialog allows selecting one, and only one file.
#● FILE_MODE_OPEN_FILES = 1
#The dialog allows selecting multiple files.
#● FILE_MODE_OPEN_DIR = 2
#The dialog only allows selecting a directory, disallowing the selection of any file.
#● FILE_MODE_OPEN_ANY = 3
#The dialog allows selecting one file or directory.
#● FILE_MODE_SAVE_FILE = 4
#The dialog will warn when a file exists.
#
@export_enum("Open file:0","Open files:1", "Open Directory:2", "Open any:3","Save file:4") var file_mode:int = 0

func _ready():
	var F = $FileDialog
	F.file_mode = file_mode


func _on_file_dialog_file_selected(path):
	if path:
		selected.emit(path)


func _on_file_dialog_dir_selected(dir):
	if dir:
		selected.emit(dir)
