extends PopupPanel
signal cancelled()
signal confirmed(text)
signal done(was_confirmed, text)
signal text_changed(text)

@onready var lbl_prompt = %lbl_prompt
@onready var line_filename = %line_filename
@onready var btn_cancel = %btn_cancel
@onready var btn_confirm = %btn_confirm

@export var text_is_valid:bool = true:
	set(new):
		if btn_confirm:
			btn_confirm.disabled = not new
		text_is_valid = new

@export var prompt:String = "Enter":
	set(new):
		if lbl_prompt:
			lbl_prompt.text = new
		prompt = new

@export var text:String = "":
	set(new):
		if line_filename:
			line_filename.text = ""
		text = new
	get:
		return line_filename.text if line_filename else text

func _ready():
	text_is_valid = text_is_valid
	prompt = prompt
	text = text

func _on_close_requested():
	pass # Replace with function body.


func _on_line_filename_text_changed(new_text):
	text_changed.emit(new_text)


func _on_btns_done(confirmed):
	done.emit(confirmed, text)
