extends HBoxContainer

signal done(confirmed)
signal confirmed()
signal cancelled()

@export var can_cancel:bool = true:
	set(new):
		var button:Button = %btn_cancel
		if button:
			button.disabled = not new
		can_cancel = new

@export var cancel_text:String = "Cancel":
	set(new):
		var button:Button = %btn_cancel
		if button:
			button.text = new
		cancel_text = new

@export var can_confirm:bool = true:
	set(new):
		var button:Button = %btn_confirm
		if button:
			button.disabled = not new
		can_confirm = new

@export var confirm_text:String = "Confirm":
	set(new):
		var button:Button = %btn_confirm
		if button:
			button.text = new
		confirm_text = new

func _ready():
	can_confirm = can_confirm
	can_cancel = can_cancel
	confirm_text = confirm_text
	cancel_text = cancel_text

func _on_btn_cancel_pressed():
	cancel()

func _on_btn_confirm_pressed():
	confirm()

func confirm():
	confirmed.emit()
	done.emit(true)

func cancel():
	cancelled.emit()
	done.emit(false)

func if_allowed_confirm():
	if can_confirm:
		confirm()

func if_allowed_cancel():
	if can_cancel:
		cancel()
