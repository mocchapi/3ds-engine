extends EditorNode
class_name scnNode
## Base node for all scene editor nodes
## [br]Each node should corrospond 1:1 to a lua node


func get_lua_node_path()->String:
	## Returns the equivalent LUA node path (in export terms)
	return "engine/nodes/node.script"


func get_viewnode()->EditorViewNode:
	## Returns an instance of an EditorViewNode that represents this object visually (including gizmos)
	return EditorViewNode.new()
