extends Node

const ProjectsList:String = "user://projects.json"

const EngineLuaDirectory:String = "res://src/lua/romfs/"

const v_patch := 0
const v_minor := 1
const v_major := 0

var v_all = v_major*1000_000_000 + v_minor*1000_000 + v_patch * 1000
var version:String = "{0}.{1}.{2}".format([v_major, v_minor, v_patch])
