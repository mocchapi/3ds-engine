extends Node
## Handles scene switching

## Create an editor environment & switch to that new scene  
## The current root scene will get freed (so make sure everything is saved!)
func edit_project(project:Project):
	Logger.line(Logger.LEVEL.INFO, "darkgreen")
	Logger.info("Switching workspace to Project editor:")
	Logger.infoP(project.config.info_name)
	Logger.infoE("at "+project.path)
	var scene = preload("res://src/engine/GUI/scenes/main/main.tscn")
	var instance = scene.instantiate()
	instance.project = project
	var root = get_tree().root
	Logger.line(Logger.LEVEL.INFO, "darkgreen")
	
	var old_root = root.get_child(-1)
	root.add_child(instance)
	root.move_child(instance, 0)
	root.remove_child(old_root)
	old_root.queue_free()

func project_manager():
	var root = get_tree().root
	root.get_child(0).queue_free()
	get_tree().change_scene_to_file("res://src/engine/GUI/scenes/projectmanager/projectmanager.tscn")
