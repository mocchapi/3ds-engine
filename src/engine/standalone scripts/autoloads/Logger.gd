extends Node
## Simple logger

enum LEVEL {
	DEBUG,
	WARNING,
	ERROR,
	INFO
}

var func_pad = 25 ## Amount of space padding for function name
var file_pad = 20 ## Amount of space padding for file name
var line_pad = 3  ## Amount of space padding for line numbers
var level_pad = 7 ## Amount of space padding for logging level

var segment_indent:String = ""	## Characters to prefix each segment with
var segments:Array = []			## Currently open segments

var point_char = "* "
var extend_char = "|＿＿ "

var minimum_log_level = LEVEL.DEBUG

var prev_details := {}

# black, red, green, yellow, blue, magenta, pink, purple, cyan, white, orange, gray.
const lvl_colors:Array = [
	'gray',
	'orange',
	'red',
	'white'
]

func rawlog(log_level:LEVEL, file_name:String, line:int, function_name:String, message=""):
	var fdict :Dictionary = {
		'level': LEVEL.keys()[log_level].rpad(level_pad),
		'file': file_name.lpad(file_pad),
		'line': str(line).rpad(line_pad),
		'function': function_name.rpad(func_pad, '.'),
		'message':message,
		
		'indent': getindent(),
		'color': lvl_colors[log_level]
	}
#	var text:String = "[{level}] ({file}:{line}) {function}: {message}".format(fdict)

	var richtext:String = "[bgcolor={color}][color=black][b][lb]{level}[rb][/b][/color][/bgcolor] [b]{file}[/b]:{line} [code]{function}[/code][color=black][bgcolor={color}]:[/bgcolor][/color]{indent} [b]{message}[/b]".format(fdict)
	print_rich(richtext)

## Draw a horizontal line of color in the terminal
func line(level:LEVEL=LEVEL.INFO, color:String=""):
	if color == "":
		color = lvl_colors[level]
	var line = "[bgcolor={color}] {pad_lvl}{pad_file} {pad_line} {pad_func} [/bgcolor][bgcolor={lvl_color}]|[/bgcolor]".format({
		'pad_lvl': " ".repeat(level_pad+2),
		'pad_file': " ".repeat(file_pad),
		'pad_func': " ".repeat(func_pad),
		'pad_line': " ".repeat(line_pad),
		'lvl_color': lvl_colors[level],
		'color': color,
	})
	print_rich(line)

## Basic logging. only includes log level & message
func basic(log_level:LEVEL, message=""):
	var fdict :Dictionary = {
		'level': LEVEL.keys()[log_level].rpad(level_pad),
		'spacer': " ".repeat(file_pad),
		'spacer2': " ".repeat(line_pad+func_pad),
		'message':message,
		'indent': getindent(),
		'color': lvl_colors[log_level]
	}
	var richtext:String = "[bgcolor={color}][color=black][b][lb]{level}[rb][/b][/color][/bgcolor]  {spacer}: {spacer2}:{indent} [b]{message}[/b]".format(fdict)
	print_rich(richtext)

## Similar to basic, but does not have the [INFO  ] or other level prefix  
## [br] Can be used without arguments to print a mostly empty line
func empty(message="", log_level:LEVEL=LEVEL.INFO):
	var line = "  {pad_lvl}{pad_file}:{pad_line} [code]{pad_func}[/code]: {message}".format({
		'pad_lvl': " ".repeat(level_pad+2),
		'pad_file': " ".repeat(file_pad),
		'pad_func': " ".repeat(func_pad),
		'pad_line': " ".repeat(line_pad),
		'message': str(message),
	})
	print_rich(line)
#	basic(log_l/evel, "")

func _log(log_level:LEVEL, message, skips:int=1):
	## Extracts names & lines from the stack instead of having to manually fill it in  
	## In release mode, this triggers a log of only the level & messaage
	if not OS.is_debug_build():
		basic(log_level, message)
		return
	var deets = get_details(skips)
	var file_name = deets['source'].split('/')[-1]
	var line = deets['line']
	var function_name = deets['function']
	if not prev_details.is_empty() and log_level not in [LEVEL.ERROR, LEVEL.WARNING]:
		if prev_details['source'] == deets['source']:
			file_name = " ".repeat(max(len(file_name),file_pad))
		if prev_details['source'] == deets['source'] and prev_details['function'] == deets['function']:
			function_name = " ".repeat(max(len(function_name), func_pad))

	prev_details = deets
	rawlog(log_level, file_name, line, function_name, message)

static func get_details(skips:int=1)->Dictionary:
	return get_stack()[skips]

## Start an indented segment for the logs that follow after this
func startseg(segment_name:String=""):
	segments.append(segment_name)

## Close one or more previous segments
func endseg(amount:int=1):
	var x = amount
	while x > 0:
		if not segments.is_empty():
			segments.pop_back()
			x -= 1
		else:
			_log(LEVEL.WARNING, "No open segments", 3)
			break

func getindent()->String:
	return segment_indent.repeat(len(segments))

## Info-level log message. Includes filename, line number, & function name
func info(message=""):
	_log(LEVEL.INFO, message, 3)

## warning-level log message. Includes filename, line number, & function name
func warning(message=""):
	_log(LEVEL.WARNING, message, 3)

## error-level log message. Includes filename, line number, & function name
func error(message=""):
	_log(LEVEL.ERROR, message, 3)

## debug-level log message. Includes filename, line number, & function name
func debug(message=""):
	_log(LEVEL.DEBUG, message, 3)



## info-level log message. Includes filename, line number, & function name  
## [br] Point: adds a "* " prefix
func infoP(message=""):
	_log(LEVEL.INFO, point_char + str(message), 3)

## warning-level log message. Includes filename, line number, & function name
## [br] Point: adds a "* " prefix
func warningP(message=""):
	_log(LEVEL.WARNING, point_char + str(message), 3)

## error-level log message. Includes filename, line number, & function name
## [br] Point: adds a "* " prefix
func errorP(message=""):
	_log(LEVEL.ERROR, point_char + str(message), 3)

## debug-level log message. Includes filename, line number, & function name
## [br] Point: adds a "* " prefix
func debugP(message=""):
	_log(LEVEL.DEBUG, point_char + str(message), 3)




## info-level log message. Includes filename, line number, & function name  
## [br] Extends: adds a "* " prefix
func infoE(message=""):
	_log(LEVEL.INFO, extend_char + str(message), 3)

## warning-level log message. Includes filename, line number, & function name
## [br] Extends: adds a "|__ " prefix
func warningE(message=""):
	_log(LEVEL.WARNING, extend_char + str(message), 3)

## error-level log message. Includes filename, line number, & function name
## [br] Extends: adds a "|__ " prefix
func errorE(message=""):
	_log(LEVEL.ERROR, extend_char + str(message), 3)

## debug-level log message. Includes filename, line number, & function name
## [br] Extends: adds a "|__ " prefix
func debugE(message=""):
	_log(LEVEL.DEBUG, extend_char + str(message), 3)



# Basic variants
func infoB(message=""):
	basic(LEVEL.INFO, message)

func warningB(message=""):
	basic(LEVEL.WARNING, message)

func debugB(message=""):
	basic(LEVEL.DEBUG, message)

func errorB(message=""):
	basic(LEVEL.ERROR, message)
