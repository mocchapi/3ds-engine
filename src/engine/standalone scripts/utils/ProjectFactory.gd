extends RefCounted
class_name ProjectFactory

## Sets up standard project structures & creates new project.json's

const directories_to_make := [
	'source',		# All the project files that have to end up in the ROMFS
	'source/assets',
	'source/assets/textures',
	'source/assets/audio',
	'source/scripts',
	'source/scenes',
	
	'misc',			# Misc files that are also editable in the editor, like pipeline icons
	'pipelines',	# Pipeline json files
	'plugins',		# Editor (godot) plugins
	'staging',		# temporary files for builds
	'patches',		# files that overwrite anyhting on romfs. useful for engine mods/patches
	'patches/mods'	# auto-executed lua files
]
## Creates folder structure & places all the default files
static func create(path:String, config:Project.ProjectConfig)->Project:
	if not path.ends_with('/'):
		path += '/'
	
	Logger.info("Creating project "+config.info_name+' at '+path)
	# Write the project to path/project.json
	var project = Project.new(path, config)
	if not project.save():
		return null
	# Create all the standard directories
	for item in directories_to_make:
		DirAccess.make_dir_recursive_absolute(path + item)
	
	# Add new project to the editor projects list
	FileUtils.write_JSON('user://projects.json', 
		[path + 'project.json'] + FileUtils.read_or_create_JSON(Constants.ProjectsList, []),
	)
	return project

## Loads a project & puts it 
static func load(path_to_project_file:String)->Project:
	var out := Project.new()
	out.load(path_to_project_file)
	return out
