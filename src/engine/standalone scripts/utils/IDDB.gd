extends RefCounted
class_name IDDB
## 3DS "official software" ID database  
## [i]I think if we stick to the 0x0005* sequence there will be no collisions, so this may not be needed[/i]

static func generate()->String:
	return "0x005" + str(abs(randi_range(1,99999999))).pad_zeros(12)

static func check(id:String)->bool:
	return true
