extends RefCounted
class_name FileUtils

static func read_or_create(path:String, content_on_create:String)->String:
	if FileAccess.file_exists(path):
		return read(path)
	else:
		write(path, content_on_create)
		return content_on_create

static func copy(source:String, destination:String):
	if exists(source, true, true):
		DirAccess.copy_absolute(source, destination)

static func read(path:String)->String:
	return FileAccess.get_file_as_string(path)

static func write(path:String, content:String)->bool:
	var file := FileAccess.open(path, FileAccess.WRITE)
	if file == null:
		return false
	file.store_string(content)
	file.close()
	return true

static func write_JSON(path:String, any, indent:="")->bool:
	return write(path, JSON.stringify(any, indent))

static func read_JSON(path:String):
	return JSON.parse_string(read(path))

static func read_or_create_JSON(path:String, object_on_create):
	if FileAccess.file_exists(path):
		return read_JSON(path)
	else:
		write_JSON(path, object_on_create)
		return object_on_create

static func exists(path:String, as_dir:bool=true, as_file:bool=true)->bool:
	if as_file and FileAccess.file_exists(path):
		return true
	elif as_dir and DirAccess.dir_exists_absolute(path):
		return true
	return false

static func getuser()->String:
	for item in ["USER", "USERNAME"]:
		if OS.has_environment(item):
			return OS.get_environment(item)
	return "you"

static func _walk_recurse(directory:String, write_to:Dictionary, filter:Callable):
	if not directory.ends_with('/'):
		directory += '/'
	var dir = DirAccess.open(directory)
	dir.list_dir_begin()
	var file_name = dir.get_next()
	while file_name != "":
		if dir.current_is_dir():
			file_name += '/'
			if filter.call(directory + file_name):
				var new = {}
				write_to[file_name] = new
				_walk_recurse(directory + file_name, new, filter)
		elif filter.call(directory + file_name):
			write_to[file_name] = directory + file_name
		file_name = dir.get_next()

static func _walk_flat_recurse(directory:String, write_to:Array, filter:Callable):
	if not directory.ends_with('/'):
		directory += '/'
	if not DirAccess.dir_exists_absolute(directory):
		printerr("directory "+directory+" does not exist")
		return
	var dir = DirAccess.open(directory)
	if dir == null:
		printerr(error_string(DirAccess.get_open_error()) + ' ' +str(DirAccess.get_open_error()))
		assert(false)
	dir.list_dir_begin()
	var file_name = dir.get_next()
	while file_name != "":
		if dir.current_is_dir():
			file_name += '/'
			if filter.call(directory + file_name):
				write_to.append(directory + file_name)
				_walk_flat_recurse(directory + file_name, write_to, filter)
		elif filter.call(directory + file_name):
			write_to.append(directory + file_name)
		file_name = dir.get_next()

## Recursively walk a directory & return it as a nested dictionary form
## [br]
## Directories have as value a dictionary. Directory name keys always end with /
## [br]
## Files have as value their absolute path string
static func walk(root_directory:String, filter:Callable=func (_x:String):return true)->Dictionary:
	var out := {}
	_walk_recurse(root_directory, out, filter)
	return out


## Recursively walk a directory & return it as a single flat array containing relative path strings to the given root (unless `absolute` = true)
## [br]
## Directory path strings always end with /
static func walk_flat(root_directory:String, filter:Callable=func (_x:String):return true)->Array:
	var out := []
	_walk_flat_recurse(root_directory, out, filter)
	return out
