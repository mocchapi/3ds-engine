extends Node
class_name PipelineBuilder

enum SECTION_TYPE {
	simple,
	progressbar,
	counter,
}

enum PRINT_TYPE {
	info,
	warning,
	error,
}

signal build_started()

signal new_section(name:String, type:SECTION_TYPE)		## Start a new section (IE next build step)
signal section_progress(value:float)					## Update the section progress (for progressbar this is a 0.0 → 1.0 range, for counter this is any number)
signal section_log(message:String, type:PRINT_TYPE)		## Print to the section log

signal build_ended(success:bool, message:String)		## Build has ended

var cancelling:bool = false



func cancel():
	cancelling = true
	pass

func start(stage:PipelineStager, config:Dictionary):
	build_started.emit()
	await do_build(stage, config)


func do_build(stage:PipelineStager, config:Dictionary):
	safe_section("Problem")
	safe_log("")
	Logger.error("PipelineBuilder "+str(self)+' does not implement `do_build()`!')
	safe_end(false, "PipelineBuilder does not implement `do_build()`!")


func safe_log(message:String, type:PRINT_TYPE=PRINT_TYPE.info):
	var out = func (): section_log.emit(message, type)
	out.call_deferred()

func safe_section(name:String, type:SECTION_TYPE=SECTION_TYPE.simple):
	var out = func (): new_section.emit(name, type)
	out.call_deferred()

func safe_progress(value:float):
	var out = func (): section_progress.emit(value)
	out.call_deferred()

func safe_end(success:bool=true, message:String=""):
	build_ended.emit(success, message)
