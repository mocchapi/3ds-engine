extends Resource
class_name Project
## Resource for getting info from & operating on a project

class ProjectConfig extends Resource:
	## 3DS UniqueID, must be a hexadecimal string like "0x4298F"
	@export var info_ID:String
	## Project name
	@export var info_name:String
	## Project author name
	@export var info_author:String
	## Version string, can be any arbritrary string
	@export var info_version:String
	## Relative path to icon texture
	@export var info_icon:String = "icon.png"
	
	## UTC Unix timestamp since last time this project was edited
	@export var meta_edited:int
	## UTC Unix timestamp of when this project was created
	@export var meta_created:int
	
	## Version of the engine this project was made with
	@export var engine_version:int = Constants.v_all

	const serializeables = ['info_ID', 'info_name', 'info_author', 'info_version', 'info_icon', 'meta_edited', 'meta_created', 'engine_version']
	func _init(name:String="New Project", author:String=FileUtils.getuser(),
			version:String="0.0.0", ID:String=IDDB.generate(),
			edited:int=Time.get_unix_time_from_system(), created:int=Time.get_unix_time_from_system()):
		self.info_name = name
		self.info_author = author
		self.info_version = version
		self.info_ID = ID
		self.meta_edited = edited
		self.meta_created = created
		
	
	func to_dict()->Dictionary:
		var out := {}
		for item in serializeables:
			var steps = item.split('_')
			var holder = null
			var current_dict = out
			for step in steps:
				if not current_dict.has(step):
					current_dict[step] = {}
				holder = current_dict
				current_dict = current_dict[step]
			holder[steps[-1]] = get(item)
		return out
	
	func from_dict(dict:Dictionary)->bool:
		for item in serializeables:
			var steps = item.split('_')
			var current_dict = dict
			var err = false
			for step in steps:
				if current_dict.has(step):
					current_dict = current_dict[step]
				else:
					Logger.error("Could not load project.json key: "+str(steps))
					err = true
			if err:
				continue
			self.set(item, current_dict)
		return true

## Base path of this project, IE "/home/username/Documents/my-project/
## [br]
## Always has a trailing "/"
@export var path:String
## The configuration for this project
@export var config:ProjectConfig

func _init(
		path_to_project_directory:String = "user://",
		config:ProjectConfig = ProjectConfig.new()
	):
	if not path_to_project_directory.ends_with('/'):
		path_to_project_directory += '/'
	self.path = path_to_project_directory
	self.config = config

## Write config to "project.json" in the project path
func save(save_path:String=path + 'project.json')->bool:
	return FileUtils.write_JSON(save_path, config.to_dict())

## Reload config from filesystem, overwriting its parameters [b]in-place[/b]!
func load(project_file_path:String=path + 'project.json')->void:
	var dict:Dictionary = FileUtils.read_JSON(project_file_path)
	path = project_file_path.get_base_dir() + '/'
	config.from_dict(dict)

static func filter_only_directories(path:String):
	return path.ends_with('/')

static func filter_only_files(path:String):
	return not path.ends_with('/')

func get_files(absolute:bool=false, exclude_engine_files:bool=false, filter:Callable=func (x:String)->bool: return true)->Array:
	var superfilter:Callable
	if exclude_engine_files:
		superfilter = func (path:String)->bool:
			return path.get_file() != "project.json" and filter.call(path)
	else:
		superfilter = filter
	
	var out := FileUtils.walk_flat(path, superfilter) 
	if not absolute:
		for idx in len(out):
			out[idx] = out[idx].substr(len(path))
	if not exclude_engine_files:
		out += FileUtils.walk_flat(Constants.EngineLuaDirectory)
	return out
