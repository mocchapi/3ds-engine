extends RefCounted
class_name PipelineStager

var filedb:FileDB
var scriptserv:ScriptServ

@export var uuid:String
@export var directory:String

signal file_staged(file)

func stage(files:Array, dir:String=directory):
	for file in files:
		if file is FileDB.ProjectFileProxy:
			file = file.solidify()
		if file is FileDB.ProjectFile:
			FileUtils.copy(file.get_absolute_path(), _makerel(dir)+file.path)



func _makerel(dir:String):
	if not directory.begins_with('/'):
		return directory + dir
	return dir

func stage_engine(dir:String):
	pass

func stage_assets():
	pass

func stage_one_script(script:FileDB.ProjectFile, ):
	var code = scriptserv.build(script.read(), script.path)
	var path = script.path
	if script.path.begins_with(Constants.EngineLuaDirectory):
		path = 'engine/' + script.path.substr(len(Constants.EngineLuaDirectory))
	Logger.infoP("Staging script: "+path)
	FileUtils.write(path, code)

func stage_scripts(target_directory, files_per_frame:int=50):
	var x := 0
	for file in filedb.get_files_in_directory('source/'):
		if file.type == FileDB.FILETYPE.SCRIPT:
			x += 1
			stage_one_script(file)
			if x == files_per_frame and files_per_frame > -1:
				await filedb.get_tree().process_frame

func stage_scenes():
	pass

func stage_patches():
	pass


func full_stage():
	pass
