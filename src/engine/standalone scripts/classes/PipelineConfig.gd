extends Resource
class_name PipelineConfig

signal name_changed(name:String)

@export var uuid:String
@export var name:String:
	set(new):
		name = new
		name_changed.emit(name)
@export var type:String
@export var parameters:Dictionary

var proxy:FileDB.ProjectFileProxy

func to_json()->String:
	return JSON.stringify({
		'uuid':uuid,
		'name':name,
		'type':type,
		'parameters':parameters
	}, "	")
