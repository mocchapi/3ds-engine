extends Resource
class_name PipelineType



## Resource that holds information for a pipeline type [br]
## This allows adding custom build presets for porting your project to a different platform [br]
## An instance of this resource must be registered with the [code]PipelineDB[/code] service [br]
## This is done by calling its [code]register_type()[/code] function with this type as an argument

## Name of the pipeline type. Must be unique between all types [br]
## Good idea to make this the name of the platform
@export var name:String
## Any description you want to give it [br]
## Best to keep it short, and maybe list some limitations
@export var description:String
## An icon that accompanies this pipeline type
@export var icon:Texture

## The configuration parameters a user may enter [br]
## Uses the following format: [code]{ 'option name': {'type':'string|number|path|bool', 'tooltip':'', 'default':''} }[/code]
@export var parameters:Array[PipelineTypeParameter]

## The node that's used to start a build of this type [br]
## Note that the root node MUST inherit from PipelineBuilder
@export var builder:PackedScene

func add_parameter(name:String, type:PipelineTypeParameter.TYPE, default=null, tooltip:String='', choices:Array=[]):
	if parameters.has(name):
		printerr("Overwriting previous entry for ",name,'!!!')
	if type == PipelineTypeParameter.TYPE.choice and choices.is_empty():
		printerr("Parameter ",name," is set as type `choice`, but the `choices` array is empty!")
		return
	
	var out := PipelineTypeParameter.new()
	out.name = name
	out.type = type
	out.tooltip = tooltip
	out.default = default
	out.choices = choices.duplicate(true)
	parameters.append(out)
