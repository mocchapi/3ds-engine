extends Resource
class_name PipelineTypeParameter

enum TYPE {
	string,			## Any single-line string (lineedit)
	text,			## Any multi-line string (textedit)
	boolean,		## Any on/off (checkbutton)
	integer,		## Any number (spinbox)
	float,			## Any decimal number (spinbox)
	percentage,		## Any decimal between 0.0 & 1.0 (slider)
	choice,			## One of the predefined values (selectbutton) (requires `choices` array to be set)
	project_path,	## Any file path within the project (lineedit + filepicker)
	system_path,	## Any file path on the users system (lineedit + filepicker)
}

@export var name:String
@export var type:TYPE = TYPE.string
@export var tooltip:String
@export var default:String

@export_category('Choice-specific')
@export var choices:Array

func get_default():
	match type:
		TYPE.integer:
			return default.to_int()
		TYPE.float:
			return default.to_float()
		TYPE.percentage:
			return clamp(default.to_float(), 0.0, 1.0)
		TYPE.boolean:
			return default.to_lower().strip_edges() == 'true'
		TYPE.string, TYPE.text, TYPE.project_path, TYPE.system_path:
			return default
		TYPE.choice:
			return choices[default.to_int()] if default != '' else choices[0]
		_:
			assert(false)
