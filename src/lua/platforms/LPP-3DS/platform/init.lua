-- init.lua
-- Platform module for LPP-3DS

Graphics.init()
Sound.init()
Socket.init()

Controls.enableScreen(TOP_SCREEN)
Controls.enableScreen(BOTTOM_SCREEN)

Platform.dofile('/platform/core/graphics.lua')
Platform.dofile('/platform/core/audio.lua')
Platform.dofile('/platform/core/input.lua')
Platform.dofile('/platform/core/networking.lua')
Platform.dofile('/platform/core/system.lua')