-- input.lua

function Platform.input.get_buttons(button_id)
    return {
        a = false,
        b = true,
        x = false,
        y = false,
        l = true,
        zl = true,
        r = true,
        zr = true,
        start = true,
        select = false,
        home = false,
        power = false,
    }
end

-- Return normalized Vector position of the 3DS circlepad
function Platform.input.get_circlepad()

end

-- Return normalized vector position of the 3DS D-pad
function Platform.input.get_dpad()
    
end

-- Return nil or a Vector position of where the bottom screen is getting touched
function Platform.input.get_touch()

end

-- Return normalized Vector position of the New 3DS C-Stick
function Platform.input.get_nipple()
    
end