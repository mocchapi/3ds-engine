-- audio.lua
-- bindings to the LPP-3DS sound api


function Platform.audio.get_volume()
    return Controls.getVolume()
end

function Platform.audio.get_aux_inserted()
    return Controls.headsetStatus()
end

function Platform.audio.load_sound(sound_path)
end

function Platform.audio.free_sound(sound_id)
end

function Platform.audio.play_sound(sound_id)
end

function Platform.audio.pause_soudn(sound_id)
end