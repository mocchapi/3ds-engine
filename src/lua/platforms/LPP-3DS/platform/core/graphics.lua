-- graphics.lua
-- bindings to the LPP-3DS graphics api

-- Start rendering the frame (set correct gpu blend)
function Platform.graphics.start_frame(screen, eye)
end

-- Stop rendering the frame (term gpu blend)
function Platform.graphics.end_frame(screen, eye)
end


-- load the image at the given path, return any kind of ID to reference it in later calls
function Platform.graphics.load_image(image_path)
    return Platform.guts.apis.Graphics.loadImage(Platform.translate_path(image_path))
end

-- draw the referenced image according to the given DrawInstruction
function Platform.graphics.draw_image(image_id, screen, eye, x_position, y_position, x_scale, y_scale, x_size, y_size, rotation)
    return Platform.guts.apis.Graphics.drawImageExtended(x_position, y_position, 0, 0, x_size, y_size, rotation, x_scale, y_scale, image_id)
end

function Platform.graphics.free_image(image_id)
    return Platform.guts.apis.Graphics.freeImage(image_id)
end

function Platform.graphics.get_image_size(image_id)
    return Platform.guts.apis.Graphics.getImageWidth(image_id), Platform.guts.apis.Graphics.getImageHeigth(image_id)
end



function Platform.graphics.disable_screen(screen_id)
    Controls.disableScreen(screen_id)
end

function Platform.graphics.enable_screen(screen_id)
    Controls.enableScreen(screen_id)
end