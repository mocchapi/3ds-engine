--

Platform = {
    networking = {},
    graphics = {},
    audio = {},
    input = {},
    system = {},

    guts = {apis = {Graphics = Graphics, Sound = Sound, Socket = Socket, }},

    on = {},
}

-- Turns project paths into platform paths. IE "source/scripts/myscript.script" -> "romfs:/source/scripts/myscript.script"
function Platform.guts.translate_path(path)
    return 'romfs:/' .. path
end

-- same as lua dofile() in function, but uses translate_path
function Platform.dofile(lua_path)
    return dofile(Platform.guts.translate_path(lua_path))
end


-- startup & exit. stopdown?

-- Called by the engine init.lua
function Platform.init()
    Platform.init = nil -- Avoid user code re-initiating the platform
    Platform.dofile("/platform/init.lua")
end

-- Pause game execution & return to home menu
function Platform.go_home()
end

-- De-init & close game
function Platform.exit()
    Graphics.term()
    Sound.term()
    Socket.term()
end