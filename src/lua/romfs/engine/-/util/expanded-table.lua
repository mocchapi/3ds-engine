-- expanded-table.lua
-- extra table functions

-- Place all values of `secondary_table` onto `primary_table` (in-place: modifies & returns `primary_table`)
function table.override(primary_table, secondary_table)
    for key, value in pairs(secondary_table) do
        primary_table[key] = value
    end
    return primary_table
end

-- Similar to table.override(), but creates a new table & returns that instead (leaving both input tables untouched)
function table.merge(primary_table, secondary_table)
    local out = {}
    for key, value in pairs(primary_table) do
        out[key] = value
    end
    for key, value in pairs(secondary_table) do
        out[key] = value
    end
    return out
end

-- Basic table search. returns integer array index or string table key (or nil) 
function table.find(haystack, needle)
	for idx, table_value in pairs(haystack) do
		if (table_value == needle) then
			return idx
		end
	end
end

-- Performs a basic table search, and returns True if the `needle` value exists inside the `haystack` table
function table.contains(haystack, needle)
	return table.find(haystack, needle) ~= nil
end

function table.haskey(table, key)
    return table[key] ~= nil
end

-- Returns a shallow copy of the given table; nestes tables remain the same reference
function table.duplicate(list)
    local out = {}
    for key, value in list do
        out[key] = value
    end
    return out
end

-- Similar to table.duplicate, but performs a deep copy where all nested tables are made unique
function table.clone(list)
    local out = {}
    for key, value in list do
        if (_type or type)(value) == 'table' then
            value = table.clone(value)
        end
        out[key] = value
    end
    return out
end