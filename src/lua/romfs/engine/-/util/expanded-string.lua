-- expanded-string.lua
-- extra string functions

function string.endswith(s, end_string)
    return string.match(s, end_string .. '$', 1)
end