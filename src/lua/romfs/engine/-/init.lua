-- init.lua
-- This is the first file that should get run by the platform entry point 
-- Its job is to initialize all engine modules & get stuff set up
-- Starting playback of the game should get handed off to another module once init are complete

-- MODULE LOAD ORDER:
-- (receives control from) platform entrypoint
-- class-sys
-- base
-- types
-- platform
-- resource constructors
--
-- config
-- 
-- mainloop (hand off control)
-- tree
-- first scene 

Platform.dofile("engine/-/util/expanded-table.lua")
Platform.dofile("engine/-/util/expanded-string.lua")

Platform.dofile("engine/-/core/class-sys.lua")
Platform.dofile("engine/-/core/base.lua")

Platform.dofile("engine/types/Color.lua")
Platform.dofile("engine/types/Signal.lua")
Platform.dofile("engine/types/Vector.lua")

Platform.init()

Platform.dofile("engine/-/core/mainloop.lua")
Platform.dofile("engine/-/core/tree.lua")