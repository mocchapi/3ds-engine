-- class-sys.lua
-- A strange little implementation of psuedo classes for lua


-- Sets the `__meta__` property on a table to link it into the pseudo class system
function Engine.guts.asobj(table, class_name, extends, super, ancestors)
	if (table.__meta__ == nil) then
		table.__meta__ = {}
	end
	table.__meta__.exports = table.__meta__.exports or {}
	table.__meta__.class = class_name -- Class name string
	table.__meta__.extends = extends or ""-- Direct ancestor
	if super then
		table.__meta__.super = super or nil
	end
	if ancestors then
		table.__meta__.ancestors = ancestors 
	end
	return table
end

ROOTTABLE = {
	__tostring = function(self)
		if self._tostring then
			return self:_tostring()
		else
			return  '<' .. (((self.__meta__ or {}).class) or '???') .. '>'
		end
	end,
	__mul = function(a, b)
		if a._multiply then
			return a._multiply(a,b) -- self, other
		elseif b._multiply then
			return b._multiply(b,a) -- self, other
		end
		error("_multiply() not implemented")
	end,
}

_type = type
-- Modified type() function that works with the pseudo class system
-- Returns class name if the given object is a pseudo class instance, otherwise returns default type() output
type = function(obj)
	local val = _type(obj)
	if (val == "table") then
		return obj.__meta__.class or "table"
	end
	return val
end

-- Constructs an instance. Don't use this directly, instead create a constructor by returning the output of CLASS
function Engine.guts.construct(class_name, table_callable, overwrites_table)
	local to_export = {}
	local to_extend = ""
	local to_signal = {}
	local solidified = false

	local func_extends = function(script_name)
		if solidified then
			error("cannot extend after instantiation")
		end
		to_extend = script_name
	end

	local func_export = function(variable_name, preferred_type)
		if solidified then
			error("cannot export after instantiation")
		end
		table.insert(to_export, variable_name)
	end

	local func_signal = function(signal_name)
		if solidified then
			error("cannot add signal after instantiation. use `self.on.signalname = Signal()` instead")
		end
		table.insert(to_signal, signal_name)
	end

	local func_docstring = function (variable_name, docstring)

	end


	local out = Engine.guts.asobj(
		table_callable(func_extends, func_export, func_signal, func_docstring),
		class_name,
		to_extend
	)

	-- validate exports
	-- or maybe dont. that seems more like a job for pre-compile checks

	-- get extended script here
	-- set it as metatable & combine the exports
	if to_extend ~= '' then
		local metatable = Engine.get_script(to_extend)()
		out.__meta__.super = metatable
		setmetatable(out, metatable)
		-- Extend ancestors (shared reference between complete super lineage)
		out.__meta__.ancestors = metatable.__meta__.ancestors or {}
		table.insert(out.__meta__.ancestors, class_name)
	else
		-- This is a root object (does not extend anything)
		out.__meta__.ancestors = {class_name}
		out.on = {} -- For signals (object.on.signalname.connect()). only the most ancient ancestor should have this
		setmetatable(out, ROOTTABLE)
	end

	for _,signal in ipairs(to_signal) do
		out.on[signal] = Signal()
	end

	-- now apply the overwrites
	table.override(out, overwrites_table)

	-- disable export(), extends(), & signal()s
	solidified = true

    if (type(out.new) == 'function') then
        -- Call object-specific "constructor" if it exists
        local obj_constructor = out.new
        out.new = nil
        -- delete it! bye. dont need to call that more than once
        obj_constructor(out)
    end

	return out
end


-- Define a class. Returns the constructor
-- 	class_name: string, should match filename
--	table_callable: function that returns the object table, should accept two arguments: `extend` and `export`
function CLASS(class_name, table_callable)
	return function (overwrites)
		return Engine.guts.construct(class_name, table_callable, overwrites or {})
	end
end

function instanceof(object, class_name)
	return table.contains(
		object.__meta__.ancestors,
		class_name
	)
end