-- tree.lua
-- tree holds the current scene and resources connected to it
-- its job is to cache resources and scripts that the nodes in the current root scene request
-- and free them when the scene gets changed
-- it holds a single root node, which is what all other nodes are children of



Engine.tree = {
    guts = {},
    resources = {
        script = {},
        image = {},
        audio = {},
    }, -- path = instance cache of resources
    current_scene_path = "",
    root = {},
}

function Engine.tree.guts.purge_resources()
end

function Engine.tree.guts.remove_root()
end

function Engine.tree.switch_scene(scene_path)

end

return Engine.tree