-- base.lua
-- Loaded early on in the init process

Engine = {
	loop = {},   -- mainloop instance
	tree = {},   -- tree instance

	guts = { -- internal stuff the user should not mess with
		resources = {
			script = {},
			image = {},
			audio = {},
		},
	},

	config = {}, -- 
	info = {},   --

	on = {} -- signals
}


function Engine.get_script(script_path, persist)
	local out = Engine.guts.resources.script[script_path] or Engine.tree.resources.script[script_path]

	if out == nil then
		out = Platform.dofile(script_path)
		if persist then
			Engine.guts.resources.script[script_path] = out
		else
			Engine.tree.resources.script[script_path] = out
		end
	end

	return out
end

function Engine.get_image(image_path, persist)
	local out = Engine.guts.resources.image[image_path] or Engine.tree.resources.image[image_path]

	if out == nil then
		out = Engine.get_script("engine/resources/Image.script")({
			id = Platform.graphics.load_image(image_path)
		})

		if persist then
			Engine.guts.resources.image[image_path] = out
		else
			Engine.tree.resources.image[image_path] = out
		end
	end
	
	return out
end

function Engine.get_sound(sound_path, persist)
	local out = Engine.guts.resources.audio[sound_path] or Engine.tree.resources.audio[sound_path]

	if out == nil then
		out = Engine.get_script("engine/resources/Sound.script")({
			id = Platform.audio.load_sound(sound_path)
		})

		if persist then
			Engine.guts.resources.audio[sound_path] = out
		else
			Engine.tree.resources.audio[sound_path] = out
		end
	end
	
	return out
end

function Engine.get_input()
end

return Engine