-- mainloop
-- gets handed control from index.lua after init
-- handles the actual running of the game for the rest of the duration of the program
-- does stuff like calling process() and draw(), keeping time, waiting on screen buffer, pausing, etc etc

Engine.loop = {
    guts = { -- Internal stuff, should not be touched by the user
        deferring = {},  -- List of callables to run at the end of the frame (after __draw, before flipsync)
        started = false, -- Has the mainloop started
    },

    on = {
        start = Signal(),        -- Emitted when Engine.loop.start() is called

        frame_start = Signal(),  -- Emitted right after the current new frame starts
        process = Signal(),      -- Emitted right before starting the process loop
        draw = Signal(),         -- Emitted right before starting the draw loop
        defer = Signal(),        -- Emitted right before starting the defer loop
        frame_end = Signal(),    -- Emitted after all other frame tasks are done
    },

    frame = 0,
}


function Engine.loop.defer(callable)
    table.insert(Engine.loop.guts.deferring, callable)
end


function Engine.loop.start()
    if not Engine.loop.guts.started then
        Engine.loop.guts.started = true
    end
end

-- The main loop that runs all the tiiiime
function Engine.loop.guts.loop()
end

function Engine.loop.guts.frame()
    
end


function Engine.loop.drawtick()
end

function Engine.loop.processtick()
end

return Engine.loop