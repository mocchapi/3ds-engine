local __vecmeta =  {
	__mul = function(a,b)
		local var aType = type(a)
		local var bType = type(b)

		if aType == 'Vector' then
			if bType == 'number' then
				return Vector(a.x * b, a.y * b)
			elseif bType == 'Vector' then
				return Vector(a.x * b.x, a.y * b.y)
			end
		elseif aType == 'number' then
			assert(bType == 'Vector')
			return Vector(b.x * a, b.y * a)
		end
	end,
	__add = function(a,b)
		assert(type(a) == 'Vector')
		assert(type(b) == 'Vector')
		return Vector(a.x + b.x, a.y + b.y)
	end,
	__sub = function(a,b)
		assert(type(a) == 'Vector')
		assert(type(b) == 'Vector')
		return Vector(a.x - b.x, a.y - b.y)
	end,
	__div = function(a,b)
		local var aType = type(a)
		local var bType = type(b)

		if aType == 'Vector' then
			if bType == 'number' then
				return Vector(a.x / b, a.y / b)
			elseif bType == 'Vector' then
				return Vector(a.x / b.x, a.y / b.y)
			end
		elseif aType == 'number' then
			assert(bType == 'Vector')
			return Vector(b.x / a, b.y / a)
		end
	end
}

function Vector(x,y)
	local out = {
		__meta__ = {class='Vector', extends='', ancestors={'Vector'}, exports={}},
		x = x,
		y = y,

		normalize = function(self)
		end
	}

	setmetatable(out, __vecmeta)
	return out
end
return Vector
