-- Godot-like signal for efficient & responsive communication between nodes
function Signal()
	return {
		__meta__ = {class='Signal', extends='', ancestors={'Signal'}, exports={}},
		listeners = {},
		one_time_listeners = {},

		connect = function(self, callable)
			if not table.contains(self.listeners, callable) then
				table.insert(self.listeners, callable)
			end
		end,

		connect_once = function(self, callable)
			if not table.contains(self.one_time_listeners, callable) then
				table.insert(self.one_time_listeners, callable)
			end
		end,

		emit = function(self, arguments)
			for _,callable in ipairs(self.listeners) do
				callable(arguments)
			end
			for _,callable in ipairs(self.one_time_listeners) do
				callable(arguments)
			end
			self.one_time_listeners = {}
		end
	}
end
return Signal
