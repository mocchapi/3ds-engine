function Signal() return {__meta__={class="Signal"}, connect=function()end, connect_once=function()end, emit=function()end,} end
function Vector(x,y) return {__meta__={class="Vector"},x=x, y=y} end
function Color(r,g,b) return {__meta__={class="Color"},r=r,g=g,b=b} end



function table.contains(haystack, needle)
	return table.find(haystack, needle) ~= nil
end

function table.find(haystack, needle)
	for idx, table_value in pairs(haystack) do
		if (table_value == needle) then
			return idx
		end
	end
end

function line_from_traceback(traceback)
	-- if string.match(traceback, ']:%d+:') == nil then
	-- 	error( '>>>' .. traceback .. '<<<')
	-- end
	return '@>>' .. string.match(string.match(traceback, ':%d+:'), '%d+') .. '<<@'
end

function getline(traceback)
	traceback = traceback or debug.traceback(nil, 2)
	return line_from_traceback(traceback)
end


function CLASS(class_name, table_callable)
	local allowed_types = {'number', 'string', 'Vector', 'Color', 'Signal',
						  'engine/resources/Image.script', 'engine/resources/Scene.script', 'engine/resources/Sound.script'}

	local to_export = {}
	local to_extend = ""
	local to_signal = {}

	local func_extends = function(script_name)
		if to_extend ~= "" then
			error("Cannot extend '" .. script_name .. "', only one script may be extended (already extending '" .. to_extend .. "')", 2)
		elseif type(script_name) ~= "string" then
			error ("Cannot extend ' ".. script_name .. "', script name must be a string.", 2)
		end
		to_extend = script_name
	end
	local func_export = function(variable_name, variable_type)
		-- TODO: check ancestry for existing exported variables too! only needs to happen in-editor though
		for key,value in ipairs(to_export) do
			if value.name == variable_name then
				error("Variable '" .. variable_name .. "' is already being exported", 2)
			end
		end

		table.insert(to_export, {name=variable_name, type_hint=variable_type, line = getline(debug.traceback(nil, 2))} )
	end
	local func_signal = function (signal_name)
		table.insert(to_signal, signal_name)
	end

	local out = table_callable(func_extends, func_export, func_signal)

	out.__meta__ = {
		extends = to_extend,
		exports = {},
		class = class_name,
		ancestors = {},
	}

	for _,variable in ipairs(to_export) do
		local vartype
		if (out[variable.name] == nil) then
			if (variable.type_hint == nil) then
				error( variable.line .. "Exported variable '" .. variable.name .. "' must be assigned a value or have a type hint set. (try `export('" .. variable.name .. "', 'sometype')`)", 1)
			else
				vartype = variable.type_hint
			end
		else
			vartype = type(out[variable.name])
			if variable.type_hint and variable.type_hint ~= vartype then
				error( variable.line .. "Exported Variable '".. variable.name .."' is hinted to be of type '" .. variable.type_hint .. "' but was assigned a value of type '" .. vartype .. "'.", 1)
			end
		end

		if not table.contains(allowed_types, vartype) then
			error( variable.line .. "Exported variable '" .. variable.name .. "' is of unsuported type '" .. vartype .. "'.", 1)
		end

		out.__meta__.exports[variable.name] = vartype

	end

	if type(out.on) ~= 'table' then
		out.on = {}
	end

	for _,signal in ipairs(to_signal) do
		out.on[signal] = Signal()
	end

	return out

end

{{{INSERTCODE}}}
