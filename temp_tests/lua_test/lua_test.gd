extends VBoxContainer

@onready var text_converted_lua = %text_converted_lua
@onready var btn_run_lua = %btn_run_lua
@onready var text_lua_result = %text_lua_result

var template:String = """local to_export = {}
local to_extend = ""
local error = ""

local function export(name, type)
	to_export[#to_export+1] = {type = type, name = name}
end

local function extend(name)
	if (to_extend ~= "") then
		error = "Cannot extend " .. name .. ", already extending " .. to_extend
	else
		to_extend = name
	end
end

local out = {CODE}


out.__meta__ = {
	extends = to_extend,
	error = error
}
out.__meta__.exports = {}
for _,value in ipairs(to_export) do
	out.__meta__.exports[value.name] = value.type or type(out[value.name]) 
end

return out"""

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	runconvert()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_code_text_changed():
	runconvert()

func runconvert():
	var code:String = $HSplitContainer/code.text.strip_edges()
	var out = template.replace("{CODE}", code)
	
	if not code.begins_with('{'):
		out = "error('code must start with {')"
	elif not code.ends_with('}'):
		out = "error('code must end with }')"
#	while not code.begins_with('{'):
#		if len(code) == 0:
#			printerr("no {")
#			return
#		code = code.substr(1)
	
	text_converted_lua.text = out
	

func recurse(write_to:Dictionary, items:Dictionary):
	for key in items:
		var val = items[key]
		if val is LuaTable:
			var new = {}
			write_to[key] = new
			recurse(new, val.to_dictionary())
		else:
			write_to[key] = val

func _on_btn_run_lua_pressed():
	var code = text_converted_lua.text
	var ctx = LuaState.new()
	ctx.open_libraries(LuaState.LUA_BASE | LuaState.LUA_MATH | LuaState.LUA_DEBUG | LuaState.LUA_TABLE)
#	ctx.open_libraries()
	var out = ctx.do_string(code, "beep")
	
	print(out)
	if out is LuaError:
		printerr("Error!!")
		text_lua_result.text = str(out)
	else:
		var beep = {}
		recurse(beep, out.to_dictionary())
		text_lua_result.text = JSON.stringify(beep, "    ")
		
